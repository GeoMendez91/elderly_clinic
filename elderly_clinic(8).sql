-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-09-2019 a las 15:20:24
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `elderly_clinic`
--
CREATE DATABASE IF NOT EXISTS `elderly_clinic` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `elderly_clinic`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat`
--

CREATE TABLE `chat` (
  `id_chat` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_receptor` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `mensaje` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `chat`
--

INSERT INTO `chat` (`id_chat`, `id_usuario`, `id_receptor`, `fecha_hora`, `mensaje`) VALUES
(17, 1, 3, '0000-00-00 00:00:00', 'Buenos dias tengo una consulta'),
(18, 1, 3, '2019-09-01 06:03:10', 'hola');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `idcita` int(11) NOT NULL,
  `idpaciente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `idmedico` int(11) NOT NULL,
  `idestado_cita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`idcita`, `idpaciente`, `fecha`, `hora`, `idmedico`, `idestado_cita`) VALUES
(3, 6, '2019-08-26', '15:30:00', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consulta`
--

CREATE TABLE `consulta` (
  `id_consulta` int(11) NOT NULL,
  `fecha_consulta` date NOT NULL,
  `diagnosticos` text COLLATE utf8_spanish2_ci NOT NULL,
  `id_medico` int(11) NOT NULL,
  `id_examen` int(11) DEFAULT NULL,
  `id_expediente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `consulta`
--

INSERT INTO `consulta` (`id_consulta`, `fecha_consulta`, `diagnosticos`, `id_medico`, `id_examen`, `id_expediente`) VALUES
(1, '2019-09-06', 'tiene problemas de bombeo sanguineo', 1, NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `idcontacto` int(11) NOT NULL,
  `nombre_con` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_con` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(125) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion_con` text COLLATE utf8_spanish2_ci NOT NULL,
  `parentesco` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono_fijo` int(11) NOT NULL,
  `telefono_movil` int(11) NOT NULL,
  `idrol` int(11) NOT NULL DEFAULT '4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`idcontacto`, `nombre_con`, `apellido_con`, `correo`, `contrasena`, `direccion_con`, `parentesco`, `telefono_fijo`, `telefono_movil`, `idrol`) VALUES
(3, 'María', 'Hernández', '', '', 'Santa Tecla', 'Hija', 22334455, 77665544, 4),
(4, 'María', 'Somoza', '', '', 'Santa Tecla', 'Hija', 22331166, 77449911, 4),
(5, 'Gabriela', 'Molina', '', '', 'Mejicanos', 'Hermana', 22558811, 77661100, 4),
(6, 'Abigail', 'Méndez', 'prueba@test.com', '1234', 'San Salvador', 'Prima', 22448866, 77442211, 4),
(7, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 0, 0, 4),
(8, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 0, 0, 4),
(9, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 0, 0, 4),
(10, 'fgsbsb', '', 'fgsdsb', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 0, 0, 4),
(11, 'Susana', 'Barrera', 'susy@test.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Mejicanos', 'esposa', 22, 77119922, 4),
(12, 'Juan ', 'tenorio', 'juan@test.com', '81dc9bdb52d04dc20036dbd8313ed055', 'San Marcos', 'Cuñado', 22990077, 77332211, 4),
(13, 'Juan ', 'tenorio', 'juan@test.com', '81dc9bdb52d04dc20036dbd8313ed055', 'San Marcos', 'Cuñado', 22990077, 77332211, 4),
(14, 'dfsfa', 'vcvcva', 'javier@test.com', 'fcea920f7412b5da7be0cf42b8c93759', 'afddfsaf', 'vcvxvx', 77550022, 22663322, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico`
--

CREATE TABLE `diagnostico` (
  `iddiagnostico` int(11) NOT NULL,
  `diagnostico` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad`
--

CREATE TABLE `especialidad` (
  `idespecialidad` int(11) NOT NULL,
  `especialidad` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `especialidad`
--

INSERT INTO `especialidad` (`idespecialidad`, `especialidad`) VALUES
(1, 'General '),
(2, 'Cardiologo'),
(3, 'Nutricionista'),
(4, 'Ortopeda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cita`
--

CREATE TABLE `estado_cita` (
  `idestado_cita` int(11) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `estado_cita`
--

INSERT INTO `estado_cita` (`idestado_cita`, `estado`) VALUES
(1, 'Activa'),
(2, 'Cancelada'),
(3, 'Reprogramada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_exp`
--

CREATE TABLE `estado_exp` (
  `idestado_exp` int(11) NOT NULL,
  `estado_exp` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `estado_exp`
--

INSERT INTO `estado_exp` (`idestado_exp`, `estado_exp`) VALUES
(1, 'activo'),
(2, 'inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen`
--

CREATE TABLE `examen` (
  `idexamen` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `tipo` varchar(30) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expediente`
--

CREATE TABLE `expediente` (
  `idexpediente` int(11) NOT NULL,
  `n_expediente` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `idpaciente` int(11) NOT NULL,
  `iddiagnostico` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `idanexos` int(11) DEFAULT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `expediente`
--

INSERT INTO `expediente` (`idexpediente`, `n_expediente`, `idpaciente`, `iddiagnostico`, `fecha`, `idanexos`, `idestado`) VALUES
(2, '1', 5, NULL, '2019-07-31', NULL, 1),
(3, '2', 6, NULL, '2019-08-12', NULL, 1),
(4, '3', 7, NULL, '2019-08-18', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medico`
--

CREATE TABLE `medico` (
  `idmedico` int(11) NOT NULL,
  `dui_medico` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_medico` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_medico` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `idespecialidad` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `idrol` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `medico`
--

INSERT INTO `medico` (`idmedico`, `dui_medico`, `nombre_medico`, `apellido_medico`, `correo`, `contrasena`, `idespecialidad`, `estado`, `idrol`) VALUES
(1, '44588228', 'Geo', 'Mendez', 'geo@medico.com', '123', 2, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `id_paciente` int(11) NOT NULL,
  `dui` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `idsexo` int(11) NOT NULL,
  `direccion` text COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id_paciente`, `dui`, `nombre`, `apellido`, `edad`, `idsexo`, `direccion`, `telefono`) VALUES
(5, '789116474', 'José', 'Somoza', 65, 1, 'Santa Tecla', 77559933),
(6, '041396743', 'Maria', 'Cordova', 70, 2, 'Mejicanos', 77994433),
(7, '023191975', 'Armando', 'Sanchez', 70, 1, 'San Salvador', 77993322),
(8, '096315627', 'Blanca', 'Orellana', 29, 1, 'Mejicanos', 22889977);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente_contacto`
--

CREATE TABLE `paciente_contacto` (
  `id_pacienteCon` int(11) NOT NULL,
  `idpaciente` int(11) NOT NULL,
  `idcontacto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `paciente_contacto`
--

INSERT INTO `paciente_contacto` (`id_pacienteCon`, `idpaciente`, `idcontacto`) VALUES
(4, 5, 4),
(5, 6, 5),
(6, 7, 6),
(7, 8, 11),
(8, 8, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporte`
--

CREATE TABLE `reporte` (
  `id_reporte` int(11) NOT NULL,
  `reporte_descrip` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_reporte` date NOT NULL,
  `iddiagnostico` int(11) NOT NULL,
  `paciente` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(25) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `rol`) VALUES
(1, 'administrador'),
(2, 'Empleado'),
(3, 'Médico'),
(4, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sexo`
--

CREATE TABLE `sexo` (
  `idsexo` int(11) NOT NULL,
  `sexo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `sexo`
--

INSERT INTO `sexo` (`idsexo`, `sexo`) VALUES
(1, 'Masculino'),
(2, 'Femenino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `correo` varchar(125) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(125) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(125) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `correo`, `nombre`, `apellido`, `contrasena`, `id_rol`) VALUES
(1, 'geo@test.com', 'Nelson', 'Mendez', '123', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id_chat`),
  ADD KEY `usuario` (`id_usuario`);

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`idcita`),
  ADD KEY `cita_paciente` (`idpaciente`),
  ADD KEY `cit_medico` (`idmedico`),
  ADD KEY `estado_cita` (`idestado_cita`);

--
-- Indices de la tabla `consulta`
--
ALTER TABLE `consulta`
  ADD PRIMARY KEY (`id_consulta`),
  ADD KEY `medico` (`id_medico`),
  ADD KEY `examen` (`id_examen`),
  ADD KEY `consulta_expediente` (`id_expediente`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`idcontacto`);

--
-- Indices de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD PRIMARY KEY (`iddiagnostico`);

--
-- Indices de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  ADD PRIMARY KEY (`idespecialidad`);

--
-- Indices de la tabla `estado_cita`
--
ALTER TABLE `estado_cita`
  ADD PRIMARY KEY (`idestado_cita`);

--
-- Indices de la tabla `estado_exp`
--
ALTER TABLE `estado_exp`
  ADD PRIMARY KEY (`idestado_exp`);

--
-- Indices de la tabla `examen`
--
ALTER TABLE `examen`
  ADD PRIMARY KEY (`idexamen`);

--
-- Indices de la tabla `expediente`
--
ALTER TABLE `expediente`
  ADD PRIMARY KEY (`idexpediente`),
  ADD KEY `paciente` (`idpaciente`),
  ADD KEY `iddiagnostico` (`iddiagnostico`);

--
-- Indices de la tabla `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`idmedico`),
  ADD KEY `especialidad` (`idespecialidad`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id_paciente`),
  ADD KEY `paciente_sexo` (`idsexo`);

--
-- Indices de la tabla `paciente_contacto`
--
ALTER TABLE `paciente_contacto`
  ADD PRIMARY KEY (`id_pacienteCon`),
  ADD KEY `pc_paciente` (`idpaciente`),
  ADD KEY `pc_contacto` (`idcontacto`);

--
-- Indices de la tabla `reporte`
--
ALTER TABLE `reporte`
  ADD PRIMARY KEY (`id_reporte`),
  ADD KEY `id_medico` (`iddiagnostico`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `sexo`
--
ALTER TABLE `sexo`
  ADD PRIMARY KEY (`idsexo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `chat`
--
ALTER TABLE `chat`
  MODIFY `id_chat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `idcita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `consulta`
--
ALTER TABLE `consulta`
  MODIFY `id_consulta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `idcontacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  MODIFY `iddiagnostico` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  MODIFY `idespecialidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `estado_cita`
--
ALTER TABLE `estado_cita`
  MODIFY `idestado_cita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estado_exp`
--
ALTER TABLE `estado_exp`
  MODIFY `idestado_exp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `expediente`
--
ALTER TABLE `expediente`
  MODIFY `idexpediente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `medico`
--
ALTER TABLE `medico`
  MODIFY `idmedico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id_paciente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `paciente_contacto`
--
ALTER TABLE `paciente_contacto`
  MODIFY `id_pacienteCon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `reporte`
--
ALTER TABLE `reporte`
  MODIFY `id_reporte` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sexo`
--
ALTER TABLE `sexo`
  MODIFY `idsexo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `cit_medico` FOREIGN KEY (`idmedico`) REFERENCES `medico` (`idmedico`),
  ADD CONSTRAINT `cita_paciente` FOREIGN KEY (`idpaciente`) REFERENCES `paciente` (`id_paciente`),
  ADD CONSTRAINT `estado_cita` FOREIGN KEY (`idestado_cita`) REFERENCES `estado_cita` (`idestado_cita`);

--
-- Filtros para la tabla `consulta`
--
ALTER TABLE `consulta`
  ADD CONSTRAINT `consulta_expediente` FOREIGN KEY (`id_expediente`) REFERENCES `expediente` (`idexpediente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `examen` FOREIGN KEY (`id_examen`) REFERENCES `examen` (`idexamen`),
  ADD CONSTRAINT `medico` FOREIGN KEY (`id_medico`) REFERENCES `medico` (`idmedico`);

--
-- Filtros para la tabla `expediente`
--
ALTER TABLE `expediente`
  ADD CONSTRAINT `paciente` FOREIGN KEY (`idpaciente`) REFERENCES `paciente` (`id_paciente`);

--
-- Filtros para la tabla `medico`
--
ALTER TABLE `medico`
  ADD CONSTRAINT `especialidad` FOREIGN KEY (`idespecialidad`) REFERENCES `especialidad` (`idespecialidad`);

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `paciente_sexo` FOREIGN KEY (`idsexo`) REFERENCES `sexo` (`idsexo`);

--
-- Filtros para la tabla `paciente_contacto`
--
ALTER TABLE `paciente_contacto`
  ADD CONSTRAINT `pc_contacto` FOREIGN KEY (`idcontacto`) REFERENCES `contacto` (`idcontacto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pc_paciente` FOREIGN KEY (`idpaciente`) REFERENCES `paciente` (`id_paciente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reporte`
--
ALTER TABLE `reporte`
  ADD CONSTRAINT `diagnos` FOREIGN KEY (`iddiagnostico`) REFERENCES `diagnostico` (`iddiagnostico`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
