<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReporteController extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('ExpedienteModel');
	}

	
	public function index()
	{
		$data['expediente'] = $this->ExpedienteModel->getExped();
		$this->load->view('expPDF', $data);
		$l = $this->output->get_output();
		$this->load->library('lib_pdf');
		$this->dompdf->loadhtml($l);
		$this->dompdf->setPaper('A4','portrait');
		$this->dompdf->render();
		$this->dompdf->stream('expediente'.'_'.date('d,m,Y').'.pdf', array("Attachment => 1")); 
	}

}