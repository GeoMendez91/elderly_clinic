<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConsultasController extends CI_Controller {

  public function __construct()
  {
      parent::__construct();
	   if(!$this->session->userdata('login')){
	        redirect(base_url().'LoginController/login');
	    }else{
      		$this->load->model('ConsultasModel');
      	}
  }


  public function getDiagnosticos($idexpediente) {
	  $data = array(
		  'page_title' => 'diagnosticos',
		  'view' => 'consultas/consultas',
		  'data_view' => array()
	  );

	  $diagnosticos = $this->ConsultasModel->get($idexpediente);
	  $data['diagnosticos'] = $diagnosticos;
	  $data['idexpediente'] = $idexpediente;


	  $this->load->view('template/MainView', $data);
  }

  public function consultasForm($idexpediente) {
	$data = array(
		'page_title' => 'ingresar diagnostico',
		'view' => 'consultas/consultaForm',
		'data_view' => array()
	);

	$paciente = $this->ConsultasModel->getPaciente($idexpediente);
	$data['paciente'] = $paciente;

	
	$this->load->view('template/MainView', $data);
}

  public function storeDiagnostic() {
	  if ($this->input->is_ajax_request()) {
		$data = array(
			'fecha_consulta' => data('Y-m-d'),
			'diagnosticos' => $this->input->post('diagnosticos'),
			'id_medico' => $this->session->userdata('id_usuario'),
			'id_examen' => null,
			'id_expediente' => $this->input->post('id_expediente'),
		);

		if ($this->ConsultasModel->storeExpedienteConsulta($data)) {
			echo json_encode(array('success' => 1));
		} else {
			echo json_encode(array('success' => 0));
		}
	  }
  }
    
}