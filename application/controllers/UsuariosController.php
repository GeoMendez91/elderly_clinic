<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuariosController extends CI_Controller {

	
	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('login')){
            redirect(base_url().'LoginController/login');
        }else{
			$this->load->model('UsuariosModel');
		}
	}

	public function index(){
		$data = array(
			'page_title' => 'Registro de usuarios',
			'view' => 'usuario_view',
			'data_view' => array()
		);
		$usuario = $this->UsuariosModel->read();
		$data['usuario'] = $usuario;
		$this->load->view('template/MainView',$data);
	}

	public function agregar_accion(){
		$data = array(
			'page_title' => 'Agregar usuario',
			'view' => 'agregar_usuario_view',
			'data_view' => array()
		);
		$this->load->view('template/MainView',$data);
	}

	public function agregar(){
		if($this->input->is_ajax_request()){
			$data = array(
				'correo' => $this->input->post('correo'),
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'contrasena' => $this->input->post('contrasena'),
				'id_rol' => $this->input->post('id_rol')
			);

			if($this->UsuariosModel->insert($data)){
				echo json_encode(array('success' => 1));
			}else{
				echo json_encode(array('success' => 0));
			}
		}else{
			echo 'No se puede agregar';
		}
	}

	public function accion($id_usuario){
		$data = array(
			'page_title' => 'Editar usuario',
			'view' => 'act_usuario_view',
			'data_view' => array()
		);

		$usuario = $this->UsuariosModel->obtener_act($id_usuario);
		$data['usuario'] = $usuario;
		$this->load->view('template/MainView',$data);
	}

	public function editar(){
		if($this->input->is_ajax_request()){
			$data = array(
				'id_usuario' => $this->input->post('id_usuario'),
				'correo' => $this->input->post('correo'),
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'contrasena' => $this->input->post('contrasena'),
				'id_rol' => $this->input->post('id_rol')
			);

			if($this->UsuariosModel->actualizar($data)){
				echo json_encode(array('success' => 1));
			}else{
				echo json_encode(array('success' => 0));
			}
		}else{
			echo 'No se pudo actualizar';
		}
	}

	public function eliminar($id_usuario){
		$this->UsuariosModel->eliminar($id_usuario);
		$this->index();
	}


}
