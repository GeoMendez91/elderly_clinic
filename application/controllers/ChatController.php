<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChatController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('login')){
            redirect(base_url().'LoginController/login');
        }else{
            $this->load->model('ChatModel');
            $this->load->model('MedicosModel');
            $this->load->model('UsuariosModel');
        }

    }

    public function getChat()
    {
        $data = array(
            'page_title' => 'Chat',
            'view' => 'chat/chat',
            'data_view' => array()
        );

      

        $this->load->view('template/MainView', $data);
    }

public function sendMessagge()
{
    if ($this->input->is_ajax_request()) {
        $mensaje = array(
            'id_usuario' => $this->session->userdata('id_usuario'),
            'id_receptor' => $this->input->post('id_receptor'),
            'fecha_hora' => date('Y-m-d H:i:s'),
            'mensaje' => $this->input->post('mensaje')
        );

        if ($this->ChatModel->send($mensaje)) {
            echo json_encode(array('mensaje' => $mensaje));   
        }else{
            echo json_encode(array('mensaje' => 0));
        }
    }else{
        echo 'no se pudo acceder';
    }

}


public function getMessagge(){
    if ($this->input->is_ajax_request()){
        $data = array(
            'id_usuario' => $this->session->userdata('id_usuario'),
            'id_rol' => $this->session->userdata('rol'),
            'receptor' => $this->input->post('receptor')
        );
        $mensajes = $this->ChatModel->getUserMessage($data);
        echo json_encode($mensajes);
        //echo json_encode(array('mensajes' => $mensajes));
    }else {
        echo 'no se pudo acceder';
    }
}



public function getActiveUsers() {

    if ($this->input->is_ajax_request()) {

        $usuarios = $this->ChatModel->getActiveUsers();
        echo json_encode($usuarios);

    } else {
        echo 'no se puedo acceder';
    }

      
       
}

}



?>