<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CitasController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('login')){
            redirect(base_url().'LoginController/login');
        }else{
            $this->load->model('ExpedienteModel');
            $this->load->model('MedicosModel');
            $this->load->model('CitasModel');
        }

    }

    public function getCitas()
    {
    	$data = array(
    		'page_title' => 'Citas',
    		'view' => 'citas/citas',
    		'data_view' => array()
    	);
    	$citas = $this->CitasModel->getCitas();
    	$data['citas'] = $citas;

    	$this->load->view('template/MainView',$data);

    }

    public function storeCitas()
    {
    	if ($this->input->is_ajax_request()){
			
		$idpaciente = $this->CitasModel->getId($this->input->post('dui'));

		if ($idpaciente != null){
		$data = array(
    			'idpaciente' => $idpaciente->id_paciente,
    			'fecha' => $this->input->post('fecha'),
    			'hora' => $this->input->post('hora'),
				'idmedico' => $this->input->post('idmedico'),
				'idestado_cita' => 1
			);

    		if ($this->CitasModel->saveCitas($data)) {
    			echo json_encode(array('success' => 1));
    		}
			
		 }else{
			 echo json_encode(array('success' => 'no resgitrado'));
		 }

    	}else{
    		echo 'no se pudo acceder';
		}
		
	}
    

     public function updateCitas()
    {
    	if ($this->input->is_ajax_request()){
			
			$idpaciente = $this->CitasModel->getId($this->input->post('dui'));
	
			if ($idpaciente != null){
			$data = array(
				'idcita' => $this->input->post('idcita'),
					'idpaciente' => $idpaciente->id_paciente,
					'fecha' => $this->input->post('fecha'),
					'hora' => $this->input->post('hora'),
					'idmedico' => $this->input->post('idmedico'),
					'idestado_cita' => 3
				);
	
				if ($this->CitasModel->updateCitas($data)) {
					echo json_encode(array('success' => 1));
				}
				
			 }else{
				 echo json_encode(array('success' => 'no resgitrado'));
			 }
	
			}else{
				echo 'no se pudo acceder';
			}
    }

    public function cancelCitas($id)
    {
    	$this->CitasModel->cancel();
    	$this->getCitas();
    }

    public function CitasForm()
    {
    	$data = array(
    		'page_title' => 'Elderly | Formulario citas',
    		'view' => 'citas/citasForm',
    		'data_view' => array()
    	);

    	$medico = $this->MedicosModel->getMedico();
    	$data['medico'] = $medico;
    	$this->load->view('template/MainView',$data);
	}
	

	public function CitasFormUpdate($id)
    {
    	$data = array(
    		'page_title' => 'Elderly | Formulario citas',
    		'view' => 'citas/updateCita',
    		'data_view' => array()
		);
		
		$medico = $this->MedicosModel->getMedico();
    	$data['medico'] = $medico;

    	$cita = $this->CitasModel->findCitas($id);
    	$data['cita'] = $cita;
    	$this->load->view('template/MainView',$data);
    }




}
?>