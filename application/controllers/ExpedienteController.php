<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExpedienteController extends CI_Controller {

	 
    
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('login')){
            redirect(base_url().'LoginController/login');
        }else{
        $this->load->model('ExpedienteModel');
        $this->load->model('MedicosModel');
        $this->load->model('FamiliaresModel');
    }

    }


	public function index()
	{
        $data = array(
            'page_title' => 'Expediente',
            'view' => 'expediente/expediente',
            'data_view' => array()
        );
       $expedientes = $this->ExpedienteModel->getExped();
        $data['expediente'] = $expedientes;
		$this->load->view('template/MainView', $data);
	}

    public function ExpedienteForm() //ExpedienteForm
    {
        $data = array(
            'page_title' => 'Expediente',
            'view' => 'expediente/expedienteForm',
            'data_view' => array()
        );
        $sexo = $this->ExpedienteModel->getSexo();
        $data['sexo'] = $sexo;
        $this->load->view('template/MainView', $data);
    }

    public function insertarPaciente(){
        $expediente = $this->ExpedienteModel->countExpediente();

        $cantidadexp = count($expediente) + 1;

        if ($this->input->is_ajax_request()) {
            $data = array(
                'dui' => $this->input->post('dui'),
                'nombre' => $this->input->post('nombre'),
                'apellido' => $this->input->post('apellido'),
                'edad' =>  $this->input->post('edad'),
                'idsexo' => $this->input->post('idsexo'),
                'direccion' => $this->input->post('direccion'),
                'telefono' => $this->input->post('telefono')
            );

            $pacienteId = $this->ExpedienteModel->insertarPaciente($data);

                $datos = array(
                'nombre_con' => $this->input->post('nombre_con'),
                'apellido_con' => $this->input->post('apellido_con'),
                'direccion_con' =>  $this->input->post('direccion_con'),
                'correo' =>  $this->input->post('correo'),
                'contrasena' => md5($this->input->post('contrasena')),
                'parentesco' => $this->input->post('parentesco'),
                'telefono_fijo' => $this->input->post('telefono_fijo'),
                'telefono_movil' => $this->input->post('telefono_movil')
            );

                $contactoid = $this->ExpedienteModel->insertarContacto($datos);



                $dato = array(
                    'idpaciente' => $pacienteId,
                    'idcontacto' => $contactoid
                );

             if ($this->ExpedienteModel->insertarConPac($dato)) {
                 $exp = array(
                     'n_expediente' => $cantidadexp,
                     'idpaciente' => $pacienteId,
                     'fecha' => date('Y-m-d')
                 );

                if ($this->ExpedienteModel->CrearExpediente($exp)) {
                    echo json_encode(array('success' => 1));
                    
            }else{
                echo json_encode(array('success' => 0));
            }
                 
             } 
    }else{
        echo 'no se puede acceder';
    }
}

public function updatePaciente($id)
{
    $data = array(
        'idpaciente' => $this->input->post('idpaciente'),
        'dui' => $this->input->post('dui'),
        'nombre' => $this->input->post('nombre'),
        'apellido' => $this->input->post('apellido'),
        'edad' =>  $this->input->post('edad'),
        'idsexo' => $this->input->post('idsexo'),
        'direccion' => $this->input->post('direccion'),
        'telefono' => $this->input->post('telefono')
    );
}

public function deleteExpediente($id)
{
    $data = array(
        'idexpediente' => $id,
        'idestado' => 2
    );
    $this->ExpedienteModel->delete($data);
    $this->index();
}

public function updateForm()
{
    $data = array(
        'page_title' => 'Actualizar expediente',
        'view' => 'expediente/updateForm',
        'data_view' => array()
    );

    $expdiente = $this->ExpedienteController->findExpediente();
    $data['expediente'] = $expdiente;

    $this->load->view('template/MainView');
}

public function updateExpediente()
{
    
}



}

?>
