<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('LoginModel');
    }

    public function index(){
        if($this->session->userdata("login")){
            redirect(base_url().'ExpedienteController');
        }else{
            $data = array(
                'page_title' => 'Inicio',
                'view' => 'index',
                'data_view' => array()
            );
            $this->load->view('template/MainView', $data);
        }
        
    }


    public function login(){
        if($this->session->userdata("login")){
            redirect(base_url().'ExpedienteController');
        }else{
            $data = array(
                'page_title' => 'Iniciar sesión',
                'view' => 'login',
                'data_view' => array()
            );
            $this->load->view('template/MainView', $data);
        }
        
    }

    public function auth(){

        $usuario = $this->input->post('correo');
        $contrasena = $this->input->post('contrasena');
        $res = $this->LoginModel->login($usuario,$contrasena);
        if(!$res){
            redirect(base_url());
        }else{
            $data = array(
                'id_usuario' => $res[0]['id'],
                'nombre' => $res[0]['nombre'],
				'apellido' => $res[0]['apellido'],
                'correo' => $res[0]['correo'],
                'contrasena' => $res[0]['contrasena'],
                'rol' => $res[0]['rol'],
                'login' => TRUE
            );;
            $this->session->set_userdata($data);
            redirect(base_url().'ExpedienteController');
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }

}

?>