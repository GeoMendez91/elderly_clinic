<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FamiliaresController extends CI_Controller {

	
    
    
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('login')){
            redirect(base_url().'LoginController/login');
        }else{
            $this->load->model('ExpedienteModel');
            $this->load->model('MedicosModel');
            $this->load->model('FamiliaresModel');
        }

    }

    public function contactoForm($id)
    {
        $data = array(
            'page_title' => 'formulario de contactos',
            'view' => 'contacto/formulario_contacto',
            'data_view' => array()
        );
        $data['expediente'] = $id;
        $this->load->view('template/Mainview', $data);
    }

    public function agregarContacto()
    {
        if($this->input->is_ajax_request())
        {

            $data = array(
                'nombre_con' => $this->input->post('nombre_con'),
                'apellido_con' => $this->input->post('apellido_con'),
                'direccion_con' => $this->input->post('direccion_con'),
                'parentesco' => $this->input->post('parentesco'),
                'correo' => $this->input->post('correo'),
                'contrasena' => md5($this->input->post('contrasena')),
                'telefono_fijo' => $this->input->post('telefono_fijo'),
                'telefono_movil' => $this->input->post('telefono_movil')
            );

            $con = array('id' => $this->input->post('expediente'));

            $contactos = $this->FamiliaresModel->cantidad($con['id']);
            $idpaciente = $this->FamiliaresModel->findIdPaciente($con['id']);

            if (count($contactos) < 3) {
                $idcontacto = $this->FamiliaresModel->insertContacto($data);

                $paciente = $this->FamiliaresModel->getPaciente($con['id']);

                $datos = array(
                    'idpaciente' => $idpaciente->idpaciente,
                    'idcontacto' => $idcontacto
                );

                if ($this->FamiliaresModel->insertCP($datos)){
                    echo json_encode(array('success' => 1));
                }else {
                    echo json_encode(array('success' => 0));
                }
            }
        }
    }


    public function eliminarContacto($id)
    {
        $this->FamiliaresModel->deleteContacto($id);
    }


    public function UpdateForm()
    {
        $data = array(
            'page_title' => 'Formulario de actualización',
            'view' => 'contacto/formulario_update',
            'data_view' => array()
        );



        $this->load->view('template/MainView');
    }

    public function actualizarContacto($id)
    {
        if ($this->input->is_ajax_request()) {
            $data = array(
                'id_contacto' => $this->input->post('id_contacto'),
                'nombre_con' => $this->input->post('nombre_con'),
                'apellido_con' => $this->input->post('apellido_con'),
                'direccion_con' => $this->input->post('direccion_con'),
                'correo' => $this->input->post('correo'),
                'parentesco' => $this->input->post('parentesco'),
                'telefono_fijo' => $this->input->post('telefono_fijo'),
                'telefono_movil' => $this->input->post('telefono_movil')
            );

            if ($this->FamiliaresModel->updateContacto($data)) {
               echo json_encode(array('success' => 1));
            }else {
                echo json_encode(array('success' => 0));
            }
        }
    }

    public function getContacto($id)
    {
        $data = array(
            'page_title' => 'contactos',
            'view' => 'contacto/contactos',
            'data_view' => array()
        );

        $contacto = $this->FamiliaresModel->getContacto($id);
        $data['contactos'] = $contacto;
        $data['expediente'] = $id;
        $this->load->view('template/MainView', $data);
    }
       

}

    ?>