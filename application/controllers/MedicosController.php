<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MedicosController extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('login')){
            redirect(base_url().'LoginController/login');
        }else{
            $this->load->model('ExpedienteModel');
            $this->load->model('MedicosModel');
            $this->load->model('FamiliaresModel');
        }

    }

    public function ingresarMedico()
    {

        if ($this->input->is_ajax_request()) {
        
        $data = array(
            'dui_medico' => $this->input->post('dui_medico'),
            'nombre_medico' => $this->input->post('nombre_medico'),
            'apellido_medico' => $this->input->post('apellido_medico'),
            'correo' => $this->input->post('correo'),
            'contrasena' => md5($this->input->post('contrasena')),
            'idespecialidad' => $this->input->post('idespecialidad')
        );

        if ($this->MedicosModel->insertMedico($data)) {
            echo json_encode(array('success' => 1));
        }else{
            echo json_encode(array('success' => 0));
        }
    }else{
        echo "No se pudo acceder";

}
}

public function UpdateMedico()
{
    if ($this->input->is_ajax_request()) {

        $data = array(
            'idmedico' => $this->input->post('idmedico'),
            'dui_medico' => $this->input->post('dui_medico'),
            'nombre_medico' => $this->input->post('nombre_medico'),
            'apellido_medico' => $this->input->post('apellido_medico'),
            'correo' => $this->input->post('correo'),
            'idespecialidad' => $this->input->post('idespecialidad')
        );

        if ($this->MedicosModel->updateMedico($data)) {
            echo json_encode(array('success' => 1));

        }else{
            echo json_encode(array('success' => 0));
        }
    

    }else{
        echo "No se pudo acceder";
    }
}

public function getMedico()
{
    $data = array(
        'page_title' => 'Medicos',
        'view' => 'medicos/medicos',
        'data_view' => array()
    );

    $medico = $this->MedicosModel->getMedico();
    $data['medicos'] = $medico;

    $this->load->view('template/MainView',$data);
}

public function medicoForm()
{
    $data = array(
        'page_title' => 'Medicos | Formulario',
        'view' => 'medicos/medicoForm',
        'data_view' => array()
    );

    $especialidad = $this->MedicosModel->getEspecialidad();
    $data['especialidad'] = $especialidad;

    $this->load->view('template/MainView',$data);
}

public function updateForm($id)
{
    $data = array(
        'page_title' => 'Actualizar | Médico',
        'view' => 'medicos/actualizarMedico',
        'data_view' => array()
    );

    $especialidad = $this->MedicosModel->getEspecialidad();
    $data['especialidad'] = $especialidad;

    $medico = $this->MedicosModel->findMedico($id);
    $data['medico'] = $medico;

    $this->load->view('template/MainView',$data);
}

public function deleteMedico(){

    if ($this->input->is_ajax_request()) {

        $data = array(
            'idmedico' => $this->input->post('id'),
            'estado' => $this->input->post('estado'),

        );

        if ( $this->MedicosModel->eliminarMedico($data)) {
            echo json_encode(array('success' => 1));

        }else{
            echo json_encode(array('success' => 0));
        }
    }
}

public function ConsultaForm()
{
    $data = array(
        'page_title' => 'Diagnostico',
        'view' => 'consultas/consultaForm',
        'data_view' => array()
    );

    $this->load->view('template/MainView', $data);
}

public function storeDiagnostico()
{
    if ($this->input->is_ajax_request()){
        $data = array(
            'fecha_consulta' =>date('Y-m-d'),
            'diagnostico' => $this->input->post('diagnostico'),
            'id_medico' => $this->session->userdata('id_usuario'),
            'id_examen' => $this->input->post('id_examen')
        );

        if ($this->MedicosModel->saveDiagnostico()){
            echo json_encode(array('success' => 1));
        }else{
            echo json_encode(array('success' => 0));
        }
    }else{
        echo 'Nose se pudo acceder';
    }
}


}

?>
