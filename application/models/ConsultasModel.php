<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConsultasModel extends CI_Model {

public function get($idexpediente) {
    $this->db->select('consulta.*,medico.*,especialidad.*');
    $this->db->from('consulta');
    $this->db->join('medico', 'medico.idmedico = consulta.id_medico');
    $this->db->join('especialidad', 'especialidad.idespecialidad = medico.idespecialidad');
    $this->db->where('id_expediente', $idexpediente);
    $consulta = $this->db->get();
    return $consulta->result();
}


public function store($data) {
   return ($this->db->insert('consulta', $data)) ? true:false;
}

public function getPaciente($idexpediente) {
    $this->db->select('paciente.*');
    $this->db->from('paciente');
    $this->db->join('expediente', 'expediente.idpaciente = paciente.id_paciente');
    $this->db->where('expediente.idexpediente',$idexpediente);
    $paciente = $this->db->get();
    return $paciente->row();
}


}

?>