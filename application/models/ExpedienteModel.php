<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExpedienteModel extends CI_Model {


public function getExped()
{
	$this->db->select('e.idexpediente,e.n_expediente,e.idpaciente,e.iddiagnostico,e.fecha,e.idanexos,e.idestado,p.dui,p.nombre,p.apellido,p.direccion,p.edad,p.telefono');
	$this->db->from('expediente e');
	$this->db->join('paciente p', 'p.id_paciente = e.idpaciente');
	$this->db->where('idestado', 1);
	$e = $this->db->get();
	return $e->result();
}

public function getSexo()
{
	$s = $this->db->get('sexo');
	return $s->result();
}

public function insertarPaciente($data)
{
	$this->db->insert('paciente',$data);

	$pacienteid = $this->db->insert_id();
	return $pacienteid;
}

public function insertarContacto($datos)
{
	$this->db->insert('contacto',$datos);

	$contactoid = $this->db->insert_id();
	return $contactoid;
}

public function insertarConPac($dato)
{
	return($this->db->insert('paciente_contacto',$dato)) ? true:false;
}

public function CrearExpediente($exp)
{
	return($this->db->insert('expediente',$exp)) ? true:false;
}

public function countExpediente()
{	
	$exp = $this->db->get('expediente');
	return $exp->result();

}

public function delete($data)
{
	$this->db->where('idexpediente', $data['idexpediente']);
	$this->db->update('expediente', $data);
}

}

?>