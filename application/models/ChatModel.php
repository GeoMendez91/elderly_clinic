<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChatModel extends CI_Model {

	public function send($mensaje)
	{
		return($this->db->insert('chat', $mensaje)) ? true:false;
	}

	public function getActiveUsers()
	{
		$this->db->select('*');
		$this->db->from('contacto');
		$users = $this->db->get();
		return $users->result();
	}

	public function getUserMessage($data)
	{
		$this->db->select('*');
		$this->db->from('chat');
		$this->db->where('id_usuario', $data['id_usuario']);
		$this->db->where('id_receptor',$data['receptor']);
		$messe = $this->db->get();
		return $messe->result();

	}

}

?>