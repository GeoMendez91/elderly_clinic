<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CitasModel extends CI_Model {

public function getCitas()
{	
 if($this->session->userdata('rol')  == 4){
	$this->db->select('idpaciente');
	$this->db->from('paciente_contacto');
	$this->db->where('idcontacto', $this->session->userdata('id_usuario'));
	$result = $this->db->get();
	$paciente = $result->row();

}
	$this->db->select('c.idcita,c.fecha,c.hora,ec.estado,m.nombre_medico,m.apellido_medico,e.especialidad,p.nombre,p.apellido,p.dui');
	$this->db->from('citas c');
	$this->db->join('estado_cita ec', 'ec.idestado_cita = c.idestado_cita');
	$this->db->join('paciente p', 'p.id_paciente = c.idpaciente');
	$this->db->join('medico m', 'm.idmedico = c.idmedico');
	$this->db->join('especialidad e', 'e.idespecialidad = m.idespecialidad');
	if ($this->session->userdata('rol')  == 3){
		$this->db->where('c.idmedico', $this->session->userdata('id_usuario'));
	} else if($this->session->userdata('rol')  == 4){
		$this->db->where('idpaciente', $paciente->idpaciente);
	}
	
	$cita = $this->db->get();
	return $cita->result();
}

public function saveCitas($data)
{

	$fecha = date('l', strtotime($data['fecha']));
	if ($fecha == 'Sunday'){

		echo json_encode(array('success' => 'domingo'));


	}else{

	$this->db->select('idcita');
	$this->db->from('citas');
	$this->db->where('fecha',$data['fecha']);
	$this->db->where('hora', $data['hora']);
	$this->db->where('idmedico',$data['idmedico']);
	$citaNoDisponible= $this->db->get();
	$cita = $citaNoDisponible->result();

	if (count($cita) < 1){
	$this->db->select('idcita');
	$this->db->from('citas');
	$this->db->where('fecha',$data['fecha']);
	$this->db->where('hora', $data['hora']);
	$this->db->where('idpaciente',$data['idpaciente']);
	$pacienteCita = $this->db->get();
	$paciente = $pacienteCita->result();
	

	if (count($paciente) < 1){
		
		return ($this->db->insert('citas',$data)) ? true:false;

	}else{
		echo json_encode(array('success' => 'horario no disponible'));
	}

}else{
	echo json_encode(array('success' => 'ya tienes una cita'));
}
}


}

public function updateCitas($data)
{
	$fecha = date('l', strtotime($data['fecha']));
	if ($fecha == 'Sunday'){

		echo json_encode(array('success' => 'domingo'));


	}else{

	$this->db->select('idcita');
	$this->db->from('citas');
	$this->db->where('fecha',$data['fecha']);
	$this->db->where('hora', $data['hora']);
	$this->db->where('idmedico',$data['idmedico']);
	$citaNoDisponible= $this->db->get();
	$cita = $citaNoDisponible->result();

	if (count($cita) < 1){
	$this->db->select('idcita');
	$this->db->from('citas');
	$this->db->where('fecha',$data['fecha']);
	$this->db->where('hora', $data['hora']);
	$this->db->where('idpaciente',$data['idpaciente']);
	$pacienteCita = $this->db->get();
	$paciente = $pacienteCita->result();
	

	if (count($paciente) < 1){
		$this->db->where('idcita', $data['idcita']);
		 return ($this->db->update('citas',$data)) ? true:false;


	}else{
		echo json_encode(array('success' => 'horario no disponible'));
	}

}else{
	echo json_encode(array('success' => 'ya tienes una cita'));
}
}
}

public function cancel($data)
{
	$this->db->where('idcita', $data['idcita']);
	return ($this->db->update($data)) ? true:false;
}

public function getId($dui)
{
	$this->db->select('id_paciente');
	$this->db->from('paciente');
	$this->db->where('dui', $dui);
	$id = $this->db->get();
	return $id->row();
}

public function findCitas($id)
{	
	$this->db->select('c.idcita,c.fecha,c.hora,ec.estado,m.nombre_medico,m.idmedico,m.apellido_medico,e.especialidad,p.nombre,p.apellido,p.dui');
	$this->db->from('citas c');
	$this->db->join('estado_cita ec', 'ec.idestado_cita = c.idestado_cita');
	$this->db->join('paciente p', 'p.id_paciente = c.idpaciente');
	$this->db->join('medico m', 'm.idmedico = c.idmedico');
	$this->db->join('especialidad e', 'e.idespecialidad = m.idespecialidad');
	$this->db->where('idcita', $id);
	$cita = $this->db->get();
	return $cita->row();
}


}

?>