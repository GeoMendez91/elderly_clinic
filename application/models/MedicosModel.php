<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MedicosModel extends CI_Model {

public function insertMedico($data){
   return ($this->db->insert('medico',$data)) ? true:false;
}

public function eliminarMedico($data){
    $this->db->where('idmedico', $data['idmedico']);
    return ($this->db->update('medico',$data)) ? true:false;
}

public function updateMedico($data){
    $this->db->where('idmedico', $data['idmedico']);
    return $this->db->update('medico', $data) ? true:false;
}

public function getMedico()
{
    $this->db->select('m.dui_medico,m.nombre_medico,m.apellido_medico,m.idmedico,m.correo,m.contrasena,e.especialidad');
    $this->db->from('medico m');
    $this->db->join('especialidad e','e.idespecialidad = m.idespecialidad');
    $m = $this->db->get();
    return $m->result();
}

public function getEspecialidad()
{
   $e = $this->db->get('especialidad');
   return $e->result();
}

public function findMedico($id)
{
    $this->db->where('idmedico',$id);
    $medico = $this->db->get('medico');
    return $medico->row();
}






}

?>