<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FamiliaresModel extends CI_Model {

public function insertContacto($data)
{
    $this->db->insert('contacto', $data);
    $id = $this->db->insert_id();
    return $id;


}

public function cantidad($id)
{
    $this->db->select('cp.idcontacto');
    $this->db->from('expediente e');
    $this->db->join('paciente_contacto cp', 'e.idpaciente = cp.idpaciente');
    $this->db->join('contacto c', 'cp.idcontacto = c.idcontacto');
    $this->db->where('e.idexpediente', $id);
    $p = $this->db->get();
    return $p->result();

}

public function insertCP($data)
{
    return ($this->db->insert('paciente_contacto', $data)) ? true:false;
}

public function deleteContacto($id)
{
    $this->db->where('idcontacto', $id);
    return $this->db->delete('contacto');
}

public function updateContacto($data)
{
    $this->db->where('idcontacto', $data['id']);
    return ($this->db->update('contacto', $data)) ? true:false;
}

public function getPaciente($id)
{   
    $this->db->select('idpaciente');
    $this->db->from('expediente');
    $this->db->where('idexpediente', $id);
    $p = $this->db->get();
    return $p->result();

}

public function getContacto($id)
{
    $this->db->select('e.idexpediente,c.idcontacto,c.nombre_con,apellido_con,c.correo,c.direccion_con,c.parentesco,c.telefono_fijo,c.telefono_movil');
    $this->db->from('expediente e');
    $this->db->join('paciente_contacto pc', 'e.idpaciente = pc.idpaciente');
    $this->db->join('contacto c', 'c.idcontacto = pc.idcontacto');
    $this->db->where('idexpediente',$id);
    $contacto = $this->db->get();
    return $contacto->result();
}

public function findIdPaciente($id)
{
    $this->db->select('idpaciente');
    $this->db->from('expediente');
    $this->db->where('idexpediente', $id);
    $pac = $this->db->get();
    return $pac->row();

}




}

?>