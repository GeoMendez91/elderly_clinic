<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuariosModel extends CI_Model {

	public function read(){
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->join('rol r','r.id_rol = usuario.id_rol','inner');
		$usuario = $this->db->get();
		return $usuario->result();
	}

	public function insert($data){
		return($this->db->insert('usuario',$data)) ? true:false;
	}

	public function obtener_act($id_usuario){
		$this->db->where('id_usuario ='.$id_usuario);
		$usuario = $this->db->get('usuario');
		return $usuario->row();
	}

	public function actualizar($data){
		$this->db->where('id_usuario', $data['id_usuario']);
		return($this->db->update('usuario',$data)) ? true:false;
	}

	public function eliminar($id_usuario){
		$this->db->where('id_usuario',$id_usuario);
		return($this->db->delete('usuario')) ? true:false;
	}

	public function get_persona(){
		$persona = $this->db->get('persona');
		return $persona->result();
	}

	public function get_rol(){
		$rol = $this->db->get('rol');
		return $rol->result();
	}

}

?>