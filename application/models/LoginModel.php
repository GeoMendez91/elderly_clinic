<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model{
	
	public function login($usuario,$contrasena){
		$this->db->where("correo",$usuario);
		$this->db->where("contrasena",$contrasena);
		$resultados = $this->db->get("usuario");
		$res = array();

		if($resultados->num_rows() > 0){
			$user =  $resultados->row();
			 $res[] = array(
				 'id' => $user->id_usuario,
				 'nombre' => $user->nombre,
				 'apellido' => $user->apellido,
				 'correo' => $user->correo,
				 'contrasena' => $user->contrasena,
				 'rol' => $user->id_rol
			 );
			 return $res;
		}else{
				$this->db->where("correo",$usuario);
				$this->db->where("contrasena",$contrasena);
		
				$resultados = $this->db->get("medico");

				if($resultados->num_rows() > 0){
					$medico = $resultados->row();
					$res[] = array(
						'id' => $medico->idmedico,
						'nombre' => $medico->nombre_medico,
						'apellido' => $medico->apellido_medico,
						'correo' => $medico->correo,
						'contrasena' => $medico->contrasena,
						'rol' => $medico->idrol
					);
					return $res;
				}else{
				$this->db->where("correo",$usuario);
				$this->db->where("contrasena",$contrasena);
				$resultados = $this->db->get("contacto");

				if($resultados->num_rows() > 0){
					$contacto = $resultados->row();
					$res[] = array(
						'id' => $contacto->idcontacto,
						'nombre' => $contacto->nombre_con,
						'apellido' => $contacto->apellido_con,
						'correo' => $contacto->correo,
						'contrasena' => $contacto->contrasena,
						'rol' => $contacto->idrol
					);
					return $res;
				}else{

					return false;
				}
		}
	}
}

}

?>