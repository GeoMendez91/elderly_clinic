<div class="container" id="formulario">
	<h1 class="titulo"><i class="fas fa-user"></i> | Agregar usuario</h1>
	<hr>
	<form name="add_usuario" action="<?php echo base_url().'UsuariosController/agregar';?>" method="post" autocomplete="off">
		<div class="row">
			<div class="form_group col-md-6">
				<label>Correo</label>
				<input class="form-control" type="email" name="correo" id="correo" required>
				<small class="form-text text-muted">Escribe tu correo</small>
			</div>

			<div class="form-group col-md-6">
				<label>Nombre</label>
				<input class="form-control" type="text" name="nombre" id="nombre" required>
				<small class="form-text text-muted">Escribe tu nombre</small>
			</div>
		</div>

		<div class="row">
			<div class="form_group col-md-6">
				<label>Apellido</label>
				<input class="form-control" type="text" name="apellido" id="apellido" required>
				<small class="form-text text-muted">Escribe tu apellido</small>
			</div>

			<div class="form-group col-md-6">
				<label>Contraseña</label>
				<input class="form-control" type="password" name="contrasena" id="contrasena" required>
				<small class="form-text text-muted">La contraseña debe ser segura</small>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>Rol</label>
				<select class="form-control" name="id_rol" id="id_rol" required>
					<option value="">-- Seleccione un rol --</option>
					<?php foreach($this->UsuariosModel->get_rol() as $r): ?>
						<option value="<?php echo $r->id_rol;?>"><?php echo $r->rol;?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="col-md-6">
			<a href="<?php echo base_url('UsuariosController') ?>"><button class="btn btn-primary btn-block btn-lg" type="button" id="agrega_usuario"><i class="fas fa-arrow-circle-left"></i> Regresar</button>
		</div></a>

		<div class="col-md-6">
			<button class="btn btn-success btn-block btn-lg" type="button" id="agrega_usuario">Agregar <i class="fas fa-check-circle"></i> </button>
		</div>
		<div style="height:80px"></div>
	</form>
</div>
