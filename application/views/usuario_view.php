	<div class="container" id="formulario">

		<h3><i class="fas fa-users-cog verde"></i> | Usuarios</h3>
			<p>Aqui puedes ver y editar los usuarios.</p>
			<hr>
		<form name="usuario" action="<?php echo base_url().'UsuariosController/accion';?>" method="post">
			<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<div class="mb-3" id="nuevoExp"><a class="btn btn-success" href="<?php echo base_url().'UsuariosController/agregar_accion';?>">Nuevo</a></div>
				<thead class="bg-primary">
					<th>Correo</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Rol</th>
					<th>Acciones</th>
				</thead>
				<tbody>
					<?php foreach($usuario as $u): ?>
						<tr>
							<td><?=$u->correo;?></td>
							<td><?=$u->nombre;?></td>
							<td><?=$u->apellido;?></td>
							<td><?=$u->rol;?></td>
							<td class="text-center"><a href="<?php echo base_url().'UsuariosController/accion/'.$u->id_usuario;?>"><i class="fas fa-edit" title="Editar"></i></a><span style="margin:7px;"></span><a href="<?php echo base_url().'UsuarioController/eliminar/'.$u->id_usuario;?>"><i class="fas fa-trash" title="Eliminar"></i></a></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</form>
	</div>