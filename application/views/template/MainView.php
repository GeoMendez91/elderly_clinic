<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $page_title ?></title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/nivo-lightbox.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/estilos.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/datatables.min.css') ?>">
    <!-- Login resources -->

    <link rel="stylesheet" href="<?php echo base_url().'assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/login/vendor/animate/animate.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/login/vendor/css-hamburgers/hamburgers.min.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/login/vendor/select2/select2.min.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/login/css/util.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/login/css/main.css';?>">

    <!-- Login resources -->
    <script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>

    <?php if($view == 'login'){ ?>

        <style>
            body{
                background-color: #003365;
            }
        </style>

    <?php } ?>
    
</head>
<body>


    <?php $this->load->view('template/HeaderView') ?>
    <?php $this->load->view($view, $data_view) ?>
    <?php $this->load->view('template/FooterView') ?>

<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/sweetalert2.all.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-notify.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.appear.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/stellar.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/owl.carousel.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/nivo-lightbox.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/wow.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.scrollTo.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/all.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/baseurl.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/auth/login.js')?>"></script>
    <script src="<?php echo base_url('assets/js/insertajax.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/actualizarajax.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/datatables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/paginadotables.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.easyPaginate.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/chat.js') ?>"></script>

    <!-- Login scripts -->

    <script src="<?php echo base_url().'assets/login/vendor/bootstrap/js/popper.js';?>"></script>
    <script src="<?php echo base_url().'assets/login/vendor/bootstrap/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/login/vendor/select2/select2.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/login/vendor/tilt/tilt.jquery.min.js';?>"></script>

    <script>
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>

    <script src="<?php echo base_url().'assets/login/js/main.js';?>"></script>


</body>
</html>