
<header>
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="top-area">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-6">
              <p class="bold text-left"></p>
            </div>
            
            <div class="col-sm-6 col-md-6">
              <p class="bold text-right"></p>
            </div>
 
          </div>
        </div>
      </div>
      <div class="container navigation">

        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
          <a class="navbar-brand" href="index.html">
                    <img src="<?php echo base_url('assets/img/asset/logo2-01.png') ?>" alt="" height="50" />
                </a>
        </div>
        <div class="row">
        <?php if($this->session->userdata('login')){ ?>
        <div class="user">
        <p><?php echo "Bienvenido: ".$this->session->userdata('nombre')." ".$this->session->apellido ?></p>
        </div>
        <?php } ?>
        <div class="user">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">
              <li class="active"><a href="<?php echo base_url('LoginController/index') ?>">Inicio</a></li>
            <?php if($this->session->userdata('login')){ ?>
              <li><a href="<?php echo base_url('ChatController/getChat') ?>">Chat</a></li>
              <li><a href="<?php echo base_url('CitasController/getCitas') ?>">Citas</a></li>
              <?php if ($this->session->userdata('rol') == 1){ ?>
              <li><a href="<?php echo base_url('MedicosController/getMedico') ?>">Médicos</a></li>
              <?php } ?>
              <?php if ($this->session->userdata('rol') != 4){ ?>
              <li><a href="<?php echo base_url('ExpedienteController/index') ?>">Expediente</a></li>
              <?php } ?>
              <?php if ($this->session->userdata('rol') == 1){ ?>
              <li><a href="<?php echo base_url('UsuariosController') ?>">Usuarios</a></li>
              <?php } ?>
              <li><a href="<?php echo base_url('LoginController/logout')?>">Cerrar sesión</a></li>
            <?php }else{ ?>
              <li><a href="<?php echo base_url('LoginController/login')?>">Iniciar sesión</a></li>
            <?php } ?>
          </ul>
        </div>
        </div>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container -->
    </nav>

  
</header>
