<!DOCTYPE html>
<html>
<head>
	<title></title>
	
</head>
<body >
	<div class="container" id="formulario_paciente">
		<div class="col-md-12">
			<h3><i class="fas fa-file-medical verde"></i> | Apertura de expediente </h3>
			<p>Para la apertura de expediente es necesario llevar el formulario con los datos del paciente y debe registrar un contacto obligatoriamente.</p>
			<hr>
			<div class="col-md-6">
			<table class="col-md-12">
				<tbody>
					<tr>
						<th class="text-primary">Datos del paciente <i class="fas fa-user-alt text-primary"></i></th>
					</tr>
					<tr style="height: 30px"></tr>
					<tr>
						<td>DUI</td>
					</tr>
					<tr>
						<td><input type="text" id="dui" name="dui" placeholder="ingrese un DUi sin guiones" class="form-control" required="">
						<div id="duiPaciente" class="text-danger">El DUI es requerido</div>
						<div id="duiPaciente2" class="text-danger">Ingrese un DUI válido</div>
					<div class="text-danger" id="nombrePaciente2"></div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Nombre</td>
					</tr>
					<tr>
						<td><input type="text" id="nombre" name="nombre" class="form-control" required="">
						<div id="nombrePaciente" class="text-danger">El nombre es requerido</div>
						<div id="nombrePaciente2" class="text-danger">El nombre no puede contener números ni caracteres especiales</div>
					<div class="text-danger" id="nombrePaciente2"></div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Apellido</td>
					</tr>
					<tr>
						<td><input type="text" id="apellido" name="apellido" class="form-control" required="">
						<div id="apellidoPaciente" class="text-danger">El apellido es requerido</div>
						<div id="apellidoPaciente2" class="text-danger">El apellido no puede contener numeros ni caracteres especiales</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Edad</td>
					</tr>
					<tr>
						<td><input type="text" id="edad" name="edad" class="form-control" required="">
						<div id="edadPaciente" class="text-danger">La edad es requerida</div>
						<div id="edadPaciente2" class="text-danger">Ingrese una edad valida</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Sexo</td>
					</tr>
					<tr>
						<td>
							<select class="form-control" required="" id="idsexo">
								<option value="">--Seleccione una opción--</option>
								<?php foreach ($sexo as $s) { ?>
									<option value="<?php echo $s->idsexo ?>"><?php echo $s->sexo ?></option>
									<?php } ?>
							</select>
						<div id="sexoPaciente" class="text-danger">Seleccione una opción</div>
						</td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Dirección</td>
					</tr>
					<tr>
						<td><input type="text" id="direccion" name="direccion" class="form-control" required="">
						<div id="direccionPaciente" class="text-danger">La dirección es requerida</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Teléfono</td>
					</tr>
					<tr>
						<td><input type="text" id="telefono" name="telefono" class="form-control" required="">
						<div id="telefonoPaciente" class="text-danger">El teléfono es requerido</div>
						<div id="telefonoPaciente2" class="text-danger">Ingrese un número de telefono válido</div>
					</td>
					</tr>
					<tr style="height: 25px"></tr>
				</tbody>
			</table>
			</div>
			<!--Fin del formulario paciente y cominza el de contacto -->
			<div class="col-md-6">
			<table class="col-md-12">
				<tbody>
					<tr>
						<th class="text-success">Datos del Contacto <i class="fas fa-user-friends text-success"></i></th>
					</tr>
					<tr style="height: 30px"></tr>
					<tr>
						<td>Nombre</td>
					</tr>
					<tr>
						<td><input type="text" id="nombre_con" name="nombre_con" class="form-control" required="">
						<div id="nombreContacto" class="text-danger">El nombre es requerido</div>
						<div id="nombreContacto2" class="text-danger">El nombre no puede contener número ni caracteres especiales</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Apellido</td>
					</tr>
					<tr>
						<td><input type="text" id="apellido_con" name="apellido_con" class="form-control" required="">
						<div id="apellidoContacto" class="text-danger">El apellido es requerido</div>
						<div id="apellidoContacto2" class="text-danger">El apellido no puede contener numeros ni caracteres especiales</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Dirección</td>
					</tr>
					<tr>
						<td><input type="text" id="direccion_con" name="direccion_con" class="form-control" required="">
						<div id="direccionContacto" class="text-danger">La dirección es requerida</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Parentesco</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="parentesco" id="parentesco" class="form-control" required="">
							<div id="parentescoContacto" class="text-danger">El parentesco es requerido</div>
							<div id="parentescoContacto2" class="text-danger">El campo no puede tener números ni carcteres especiales</div>
						</td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Telefono Fijo</td>
					</tr>
					<tr>
						<td>
							<div class="row">
								<div class="col-md-6"><input type="text" id="telefono_fijo" name="telefono_fijo" class="form-control" required=""></div>
								<div class="col-md-6"><input type="text" id="telefono_movil" name="telefono_movil" class="form-control" required=""></div>
							</div>
						<div id="telFijo" class="text-danger">El teléfono es requerido</div>
						<div id="telFijo2" class="text-danger">Ingresa un número de teléfono válido</div></td>
						<div id="telMovil" class="text-danger">El teléfono es requerido</div>
						<div id="telMovil2" class="text-danger">Ingresa un número de teléfono válido</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>correo</td>
					</tr>
					<tr>
						<td><input type="text" id="correo" name="correo" class="form-control" required="">
						<div id="correoContacto" class="text-danger">El correo es requerido</div>
						<div id="correoContacto2" class="text-danger">Ingrese un correo válido</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Contraseña</td>
					</tr>
					<tr>
						<td><input type="password" id="contrasena" name="contrasena" class="form-control" required="">
						<div id="contraContacto" class="text-danger">La contraseña es requerida</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					

				</tbody>
			</table>
			</div>
			
		</div>
		<div class="col-md-12" id="boton">
				<input type="button" name="registrar_paciente" id="registrar_paciente" value="Registrar paciente" class="btn btn-primary btn-lg btn-block">
			</div>
		
	</div>

</body>
</html>