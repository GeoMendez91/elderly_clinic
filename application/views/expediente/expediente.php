<!DOCTYPE html>
<html>

<head>
	<title></title>
</head>
<body>
	<div class="container" id="formulario">

	<h3><i class="fas fa-file-medical verde"></i> | Expedientes </h3>
			<p>Aqui puedes ver y editar los expedientes.</p>
			<hr>
            <?php if ($this->session->rol == 2 || $this->session->rol == 1) {?>
		<div class="mb-3" id="nuevoExp"><a href="<?php echo base_url('ExpedienteController/ExpedienteForm') ?>"><button class="btn btn-success"><i class="fas fa-file-medical "></i> Crear nuevo expediente</button></a>
        </div>
    <?php } ?>
        <div class="mb-3"><a href="<?php echo base_url('ReporteController/index') ?>"><button class="btn btn-primary"><i class="fas fa-download"></i> Exportar PDF</button></a>
        </div>
	
    <table class="table col-md-6 bg-light" id="expedientes-table">
    	

    		<thead class="bg-primary">
    			<tr>
    				<th class="text-center" width="5%">N° Expediente</th>
    				<th class="text-center" width="10%">DUI</th>
    				<th class="text-center" width="20%">Paciente</th>
    				<th class="text-center" width="10%">Edad</th>
    				<th class="text-center" width="10%">Dirección</th>
    				<th class="text-center" width="10%">Teléfono</th>
    				<th class="text-center" width="10%">Fecha de apertura</th>
    				<th class="text-center" width="30%">Acciones</th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php foreach ($expediente as $e) { ?>
    			<tr>
    			<td align="center"><?php echo 
    			 $longitud = strlen($e->n_expediente);
    			switch ($longitud) { case '1': echo "0000".$e->n_expediente; break;
		    						 case '2': echo "000".$e->n_expediente; break;
		    						 case '3': echo "00".$e->n_expediente; break;
		    						 case '4': echo "0".$e->n_expediente; break;
		    						 case '5': echo $e->n_expediente; break;
		    						}

    			 ?></td>
    			<td align="center"><?php echo $e->dui ?></td>
    			<td align="center"><?php echo $e->nombre.' '.$e->apellido ?></td>
    			<td align="center"><?php echo $e->edad." años" ?></td>
    			<td align="left"><?php echo $e->direccion ?></td>
    			<td align="left"><?php echo $e->telefono ?></td>
    			<td align="left"><?php echo $e->fecha ?></td>
    			<td><a href="<?php echo base_url('ConsultasController/getDiagnosticos/').$e->idexpediente ?>"><button class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Ver diagnósticos" id="prueba"><i class="fas fa-heartbeat"></i></button></a>
						<a href="<?php echo base_url('').$e->idexpediente ?>" data-toggle="tooltip" data-placement="top" title="Ver anexos" id="prueba2"><button class="btn btn-success btn-circle" ><i class="fas fa-id-card-alt"></i></button></a></button></a>
						<a href="<?php echo base_url().$e->idexpediente ?>"><button class="btn btn-info btn-circle" data-toggle="tooltip" data-placement="top" title="Ver examenes" id="examenes"><i class="fas fa-eye"></i></button></a></button></a>
						<a href="<?php echo base_url('FamiliaresController/getContacto/').$e->idexpediente ?>"><button class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="top" title="Ver contactos" id="contactos"><i class="fas fa-users"></i> </button></a></button></a>
						<?php if ($this->session->rol == 1){ ?> 
						<a href="<?php echo base_url('ExpedienteController/deleteExpediente/').$e->idexpediente ?>"><button class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Eliminar expediente" id="eliminar"><i class="fas fa-trash"></i> </button></a></button></a></td>
				<?php	} ?>
						</tr>
						<?php } ?>
    		</tbody>

			</table>

	</div>
    <script type="text/javascript">
        $(function () {
        $("[rel='tooltip']").tooltip();
    });

        
    </script>

</body>
</html>
