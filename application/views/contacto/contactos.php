<body>
    <div class="container" id="formulario">
		<div class="col-md-12"><h3><i class="fas fa-user-friends text-primary"></i> | Contactos</h3>
		<p>En esta sección puedes consultar y registar contactos adicionales del paciente, el número maximo de contactos por paciente es de 3.</p>
		<hr></div>
		
    	<div class="mb-3" id="nuevoExp"><a href="<?php echo base_url('FamiliaresController/contactoForm/').$expediente ?>"><button class="btn btn-success"><i class="fas fa-file-medical "></i> Crear nuevo Conctacto</button></a>
		</div>
		<div><p><strong>Contactos del paciente:</strong></p></div>
        <div class="col-md-12">
    <?php foreach ($contactos as  $c) { ?>
        <table class="table col-md-6 bg-light" >
    	<thead class="bg-primary" >
            <tr style="height: 30px">
			<th colspan="3" class="text-light">Datos del contacto</th>
			<th colspan="1" class="text-light"><a href="<?php echo base_url('')?>"><button class="btn " style="background-color: #FF672A"><i class="fas fa-arrow-circle-left"></i> Regresar</button></a></th>
            </tr>
    	</thead>
				<tbody>
					<tr>
						<th class="text-left">Nombre: </th>
						<td align="left"><?php echo $c->nombre_con.' '.$c->apellido_con ?></td>
						<th class="text-left">Parentesco: </th>
						<td align="left"><?php echo $c->parentesco ?></td>
					</tr>
					<tr>
						<th class="text-left">Dirección: </th>
						<td align="left"><?php echo $c->direccion_con ?></td>
						<th class="text-left">Teléfono fijo: </th>
						<td align="left"><?php echo $c->telefono_fijo ?></td>
                    </tr>
                    <tr>
						<th class="text-left">Correo: </th>
						<td align="left"><?php echo $c->correo ?></td>
						<th class="text-left">Teléfono móvil: </th>
						<td align="left"><?php echo $c->telefono_movil ?></td>
					</tr>
					
				</tbody>
            </table>
            <?php } ?>
        </div>
        <div>
		<a href="<?php echo base_url('ExpedienteController/index')?>"><button class="btn " style="background-color: #FF672A"><i class="fas fa-arrow-circle-left"></i> Regresar</button></a>
		<br><br>
        </div>
    </div>
</body>