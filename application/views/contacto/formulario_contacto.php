<body>
	<body >
	<div class="container" id="formulario_paciente">
		<div class="col-md-12">
			<h3>Agregar nuevo Contacto </i></h3>
			<hr>
			<form>
			
			<!--Fin del formulario paciente y cominza el de contacto -->
			<div class="col-md-6">
			<table class="col-md-12">
				<tbody>
					<tr>
						<th class="text-primary">Datos del Contacto <i class="fas fa-user-friends text-success"></i></th>
					</tr>
					<tr style="height: 30px"></tr>
					<tr>
						<td>Nombre</td>
					</tr>
					<tr>
						<td><input type="text" id="nombre_con" name="nombre_con" class="form-control" required="">
						<div id="nombreContacto" class="text-danger">El nombre es requerido</div>
						<div id="nombreContacto2" class="text-danger">El nombre no puede contener número ni caracteres especiales</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Apellido</td>
					</tr>
					<tr>
						<td><input type="text" id="apellido_con" name="apellido_con" class="form-control" required="">
						<div id="apellidoContacto" class="text-danger">El apellido es requerido</div>
						<div id="apellidoContacto2" class="text-danger">El apellido no puede contener numeros ni caracteres especiales</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Dirección</td>
					</tr>
					<tr>
						<td><input type="text" id="direccion_con" name="direccion_con" class="form-control" required="">
						<div id="direccionContacto" class="text-danger">La dirección es requerida</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Parentesco</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="parentesco" id="parentesco" class="form-control" required="">
							<div id="parentescoContacto" class="text-danger">El parentesco es requerido</div>
							<div id="parentescoContacto2" class="text-danger">El campo no puede tener números ni carcteres especiales</div>
						</td>
					</tr>
					<tr style="height: 25px"></tr>
				</tbody>
			</table>
			</div>

			<div class="col-md-6">
			<table class="col-md-12">
				<tbody>
					<tr style="height: 55px"></tr>
					<tr>
						<td>Correo electrónico</td>
					</tr>
					<tr>
						<td><input type="email" id="correo" name="correo" class="form-control" placeholder="sophie@example.com" required="">
						<div id="correoContacto" class="text-danger">Ingrese un correo válido</div>
						<div id="correoContacto2" class="text-danger">Ingrese un correo válido</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Contraseña</td>
					</tr>
					<tr>
						<td><input type="password" id="contrasena" name="contrasena" class="form-control" required="">
						<div id="contraContacto" class="text-danger">Ingrese una contraseña</div></td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Telefono Fijo</td>
					</tr>
					<tr>
						<td><input type="text" id="telefono_fijo" name="telefono_fijo" class="form-control" required="">
						<div id="telFijo" class="text-danger">El número de télefono es requerido</div>
						<div id="telFijo2" class="text-danger">Ingresa un número de teléfono válido</div>
					</td>
					</tr>
					<tr style="height: 25px"></tr>
					<tr>
						<td>Teléfono Movil</td>
					</tr>
					<tr>
						<td><input type="text" id="telefono_movil" name="telefono_movil" class="form-control" required="">
						<div id="telMovil" class="text-danger">El número de télefono es requerido</div>
						<div id="telMovil2" class="text-danger">Ingresa un número de teléfono válido</div>
					</td>
					</tr>
					<tr style="height: 25px"></tr>
					<input type="hidden" id="expediente" name="expediente" value="<?php echo $expediente ?>">
				</tbody>
			</table>
			</div>
			</form>
			
		</div>
		<div class="col-md-6" id="boton">
			<button class="btn btn-secondary btn-lg btn-block"><i class="fas fa-arrow-alt-circle-left"></i> Regresar</button>
			</div>
		<div class="col-md-6" id="boton">
			<button class="btn btn-primary btn-lg btn-block" name="registrar_contacto" id="registrar_contacto">Registrar contacto <i class="fas fa-check-circle"></i></button>
			</div>
	</div>
</body>