<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Expedientes pdf</title>
	<style>
		span{
			float: left;
			margin: 0;
		}

		.footer {
			position:fixed;
			left:0px;
			bottom:0px;
			width:100%;
			background:#d0dafd;
			padding: 5px;
		}

		table {     
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 12px;    margin: 45px;     width:100%; text-align: left;    border-collapse: collapse; 
		}

		th {     
			font-size: 13px;     font-weight: normal;     padding: 8px;     background: #b9c9fe;
			border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; 
		}

		td {    
			padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;
			color: #669;    border-top: 1px solid transparent; 
		}

		tr:hover td { 
			background: #d0dafd; color: #339; 
		}
	</style>
</head>
<body>
	<strong style="align-content: center;"></strong>
	<form>

		<header>
			<div id="contenedor">
				<div>
					<h1>Expedientes Elderly Clinic</h1>	
				</div>
				<hr>
				<?php date_default_timezone_set('America/El_Salvador'); ?>
				<span>Fecha:<?=date('d/m/Y');?></span><br>
				<span>Hora:<?=date("H:i:s");?></span>
			</header>
			<br><br><br>
			<center>
				<h2>Todos nuestros expedientes</h2>			
				<div>
					<table align="center">
						<thead>
							<tr style="background-color: lightblue">
								<th>N° expediente</th>
								<th>DUI</th>
								<th>Paciente</th>
								<th>Edad</th>
								<th>Dirección</th>
								<th>Teléfono</th>
								<th>Fecha de apertura</th>
							</tr>
						</thead>
						<tbody >
							<?php foreach ($expediente as $l):?>
								<tr>
									<td><?=$l->n_expediente ?></td>
									<td><?=$l->dui ?></td>
									<td><?=$l->nombre ?></td>
									<td><?=$l->edad?></td>
									<td><?=$l->direccion ?></td>
									<td><?=$l->telefono?></td>
									<td><?=$l->fecha?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<div class="footer">
					Elderly Clinic 2019 &copy; Derechos reservados
				</div>				
			</center>
		</form>
	</body>
	</html>