<div class="container">
	<h1 class="titulo"><i class="fas fa-user-edit"></i> | Editar usuario</h1>
	<hr>
	<form name="update_usuario" action="<?php echo base_url().'UsuariosController/editar';?>" method="post">
		<div class="row">
			<div class="form-group col-md-4">
				<label>Correo</label>
				<input class="form-control" type="text" name="correo" id="correo" value="<?=$usuario->correo?>">
			</div>

			<div class="form-group col-md-4">
				<label>Nombre</label>
				<input class="form-control" type="text" name="nombre" id="nombre" value="<?=$usuario->nombre?>">
			</div>
		</div>

		<div class="row">

			<div class="form-group col-md-4">
				<label>Apellido</label>
				<input class="form-control" type="text" name="apellido" id="apellido" value="<?=$usuario->apellido?>">
			</div>

			<div class="form-group col-md-4">
				<label>Contraseña</label>
				<input class="form-control" type="text" name="contrasena" id="contrasena" value="<?=$usuario->contrasena?>">
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-4">
				<label>Rol</label>
				<select class="form-control" name="id_rol" id="id_rol" required>
					<option value="">-- Seleccione un rol --</option>
					<?php foreach($rol = $this->UsuariosModel->get_rol() as $r): ?>
						<option value="<?php echo $r->id_rol?>"<?=$r->id_rol == $usuario->id_rol ? 'selected' : ''?>><?php echo $r->rol?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>

		<div>
			<input type="hidden" name="id_usuario" id="id_usuario" value="<?=$usuario->id_usuario;?>">
		</div>

		<div>
			<button class="btn btn-primary" type="button" id="actua_usuario">Guardar cambios</button>
		</div>
	</form>
</div>