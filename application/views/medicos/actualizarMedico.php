<body>

	<div class="container" id="formulario">
	<div class="col-md-12 bg-light" >
	<h3><i class="fas fa-user-md verde"></i> | Registro de Médico</h3>
	<p>Formulario para ingresar un nuevo médico al sistema, una vez registrado el médico tendra acceso a ciertas funcionaliadades del sistema.</p>
	<form id="formularioMedico"></form>
		<table class="col-md-12">
			<tbody>
			<tr>
					<td>DUI</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="dui_medico" value="<?= $medico->dui_medico ?>" name="dui_medico" placeholder="Ingrese un DUI sin guiones" class="form-control required text" required="">
						<div id="duiMedico" class="text-danger">El DUI es requerido</div>
						<div id="duiMedico2" class="text-danger">Ingrese un DUI válido</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Nombre</td>
					<input type="hidden" id="idmedico" value="<?= $medico->idmedico ?>">
				</tr>
				<tr>
					<td>
						<input type="text" id="nombre_medico" name="nombre_medico" value="<?= $medico->nombre_medico ?>" class="form-control required text" required="">
						<div id="nombreMedico" class="text-danger">El nombre es requerido</div>
						<div id="nombreMedico2" class="text-danger">El nombre no puede contener número ni caracteres especiales</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Apellido</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="apellido_medico" name="apellido_medico" value="<?= $medico->apellido_medico ?>" class="form-control required text" required="">
						<div id="apellidoMedico" class="text-danger">El apellido es requerido</div>
						<div id="apellidoMedico2" class="text-danger">El apellido no puede contener numeros ni caracteres especiales</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Correo</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="correo" name="correo" value="<?= $medico->correo ?>" class="form-control required email" required="">
						<div id="correoMedico" class="text-danger">El correo es requerido</div>
						<div id="correoMedico2" class="text-danger">Ingrese un correo válido</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				
				<tr>
					<td>Especialidad</td>
				</tr>
				<tr>
					<td>
					<select class="form-control" id="idespecialidad">
							<option value="--Seleccione una opción--"></option>
							<?php foreach ($especialidad as $e) { 
                                  if ($e->idespecialidad == $medico->idespecialidad){  ?>
                                <option value="<?= $e->idespecialidad ?>" selected><?= $e->especialidad ?></option>
                                <?php }else{ ?>
                                    <option value="<?= $e->idespecialidad ?>"><?= $e->especialidad ?></option>
								<?php } } ?>
						</select>
						<div id="especialidadMedico" class="text-danger">Seleccione una especialidad</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>
						<div class="col-md-6">
							<a href="<?php echo base_url('MedicosController/getMedico') ?>"><button class="btn btn-block btn-lg btn-secondary col-md-12"><i class="fas fa-arrow-circle-left"></i> Regresar</button></a>
						</div>
						<div class="col-md-6">
							<button class="btn btn-block btn-lg btn-primary col-md-12" id="actualizar_medico">Actualizar <i class="fas fa-arrow-circle-right"></i></button>
						</div>	
					</td>
				</tr>
				<tr style="height: 30px"></tr>
			
			</tbody>
		</table>
		</div>
	</div>
</body>