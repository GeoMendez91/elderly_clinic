<body>

	<div class="container" id="formulario">
	<div class="col-md-12 bg-light" >
	<h3><i class="fas fa-user-md verde"></i> | Registro de Médico</h3>
	<p>Formulario para ingresar un nuevo médico al sistema, una vez registrado el médico tendra acceso a ciertas funcionaliadades del sistema.</p>
	<form id="formularioMedico"></form>
	
		<table class="col-md-12">
			<tbody>
			<tr>
					<td>DUI</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="dui_medico" name="dui_medico" placeholder="Ingrese un DUI sin guiones" class="form-control required text" required="">
						<div id="duiMedico" class="text-danger">El DUI es requerido</div>
						<div id="duiMedico2" class="text-danger">Ingrese un DUI válido</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Nombre</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="nombre_medico" name="nombre_medico" class="form-control required text" required="">
						<div id="nombreMedico" class="text-danger">El nombre es requerido</div>
						<div id="nombreMedico2" class="text-danger">El nombre no puede contener número ni caracteres especiales</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Apellido</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="apellido_medico" name="apellido_medico" class="form-control required text" required="">
						<div id="apellidoMedico" class="text-danger">El apellido es requerido</div>
						<div id="apellidoMedico2" class="text-danger">El apellido no puede contener numeros ni caracteres especiales</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Correo</td>
				</tr>
				<tr>
					<td>
						<input type="text" id="correo" name="correo" class="form-control required email" required="">
						<div id="correoMedico" class="text-danger">El correo es requerido</div>
						<div id="correoMedico2" class="text-danger">Ingrese un correo válido</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Contraseña</td>
				</tr>
				<tr>
					<td>
						<input type="password" id="contrasena" name="contrasena" class="form-control required" required="">
						<div id="contraMedico" class="text-danger">La contraseña es requerida</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>Especialidad</td>
				</tr>
				<tr>
					<td>
						<select name="idespecialidad" id="idespecialidad" class="form-control required">
							<option value="">--Seleccione un opción--</option>
							<?php foreach ($especialidad as $e) { ?>
								<option value="<?= $e->idespecialidad ?>"><?= $e->especialidad ?></option>
								<?php } ?>
						</select>
						<div id="especialidadMedico" class="text-danger">Seleccione una especialidad</div>
					</td>
				</tr>
				<tr style="height: 30px"></tr>
				<tr>
					<td>
						<div class="col-md-6">
							<button class="btn btn-block btn-lg btn-secondary col-md-12">Regresar</button>
						</div>
						<div class="col-md-6">
							<button class="btn btn-block btn-lg btn-primary col-md-12" id="registrar_medico">Registrar</button>
						</div>	
					</td>
				</tr>
				<tr style="height: 30px"></tr>
			
			</tbody>
		</table>
		</div>
	</div>
</body>
