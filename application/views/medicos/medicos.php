<body>

<div class="container" id="formulario">
<h3><i class="fas fa-stethoscope verde"></i> | Médicos </h3>
			<p>Aqui puedes ver los médicos registrados.</p>
			<hr>
		<div class="mb-3" id="nuevoExp"><a href="<?php echo base_url('MedicosController/medicoForm') ?>"><button class="btn btn-success"><i class="fas fa-file-medical "></i> Registrar Médico</button></a>
        </div>
        
        <div>
            <table class="table table-light" id="medicostab">
                <thead class="bg-primary">
                    <tr>
                        <th class="text-center">DUI</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Apellido</th>
                        <th class="text-center">Correo</th>
                        <th class="text-center">Especialidad</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody class="bg-light">
                <?php foreach ($medicos as $m) { ?>
                    <tr>
                        <td align="center"><?php echo $m->dui_medico ?></td>
                        <td align="center"><?php echo $m->nombre_medico ?></td>
                        <td align="center"><?php echo $m->apellido_medico ?></td>
                        <td align="center"><?php echo $m->correo ?></td>
                        <td align="center"><?php echo $m->especialidad ?></td>
                        <td align="center"><a href="<?php echo base_url('MedicosController/updateForm/').$m->idmedico ?>"><button class="btn btn-info btn-circle" ><i class="fas fa-sync-alt"></i></button></a> <button class="btn btn-danger btn-circle" name="estadoMedico" id="estadoMedico" onclick="cambiarEstado($this)" value="<?= $m->idmedico ?>"><i class="fas fa-trash"></i></button></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
	
</body>