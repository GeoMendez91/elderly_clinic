<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
   <div class="container" id="formulario">
      
    <div class="col-md-8">
        <div class="panel panel-info" >
            <div class="panel-heading">
                RECENT CHAT HISTORY
            </div>
            <div class="panel-body">

                    <ul class="media-list" id="message-list" style="height: 500px">
     
                      </ul>

                      
            </div>
            <div class="panel-footer">
                <div class="input-group">
                                    <input type="text" class="form-control" id="mensaje" placeholder="Enter Message" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" id="send" type="button">SEND</button>
                                    </span>
                                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
               ONLINE USERS
            </div>
            <div class="panel-body">
                <ul class="media-list" id="user-list">
                   
    
                                </ul>
                </div>
            </div>
        
    </div>
</div>
  </div>
  <script>
  window.onload = function() {
    getMessage();
    getContactos();
};
  </script>
</body>
</html>
