<body>
    <div class="container" id="formulario">
   
    <div class="col-md-12"><h3><i class="fas fa-file-medical "></i> | Elderly Clinic</h3>
		<p>En esta sección revisar el historial de diagnósticos de un paciente, tambien puedes generar un nuevo diagnóstico.</p>
		<hr></div>
<form action=""></form>        
<table class="col-md-12">
    <tr class="col-md-12">
        <th class="col-md-4">Paciente:  <?php echo $paciente->nombre." ".$paciente->apellido?> </th>
       

        <th class="col-md-4" style="width:600px"></th>
        <td> Fecha: <?php echo date('d/M/Y') ?></td>
    </tr>
    
    <tr style="height:80px"><td colspan="3"></td></tr>
    <tr>
        <td colspan="3">
           Diagnóstico:
        </td>
    </tr>
    <tr >
    <td colspan="3">
        <textarea name="diagnosticos" id="diagnosticos" cols="30" rows="10" class="form-control"></textarea>
      
        <div id="diagnostico"  class="text-danger"></div>
    </td>
    </tr>
    <tr>
    <td><span style="weight: bold">Médico: </span><?php echo $this->session->userdata('nombre')." ".$this->session->userdata('apellido') ?></td>
    
    </tr>
    <tr>
        <td>
            <button>Anexar examen</button>
        </td>
    </tr>
    <tr style="height:50px"></tr>
    <tr>
        <td colspan="3">
            <button class="btn btn-Primary btn-block btn-lg" id="save-consulta">Guardar Consulta</button>
        </td>
    </tr>

    
        </table>
    </div>
</body>


<!-- modal -->