<body>
    <div class="container" id="formulario">
    <div class="col-md-12"><h3><i class="fas fa-heartbeat text-danger"></i> | Diagnósticos</h3>
		<p>En esta sección se generan los diagnósticos de los pacientes que seran agregados a su expediente médico.</p>
		<hr></div>
		
    	<div class="mb-3" id="nuevoExp"><a href="<?php echo base_url('ConsultasController/consultasForm/').$idexpediente ?>"><button class="btn btn-success"><i class="fas fa-file-medical "></i> Generar un nuevo Diagnóstico</button></a>
        </div>
        <div>
            <table class="table table-light">
        <thead class="bg-primary">
            <tr>
                <th class="text-center">Fecha:</th>
                <th class="text-center">Diagnosticos:</th>
                <th class="text-center">Médico:</th>
                <th class="text-center">Examenes:</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($diagnosticos as $consulta) { ?>
            <tr>
                <td align="center"><?php echo $consulta->fecha_consulta ?></td>
                <td align="center"><?php echo $consulta->diagnosticos ?></td>
                <td align="center"><?php echo $consulta->nombre_medico." ".$consulta->apellido_medico." | ".$consulta->especialidad ?></td>
                <td align="center"><a href="<?php echo base_url() ?>"><button class="btn btn-info btn-circle" data-toggle="tooltip" data-placement="top" title="Ver examenes" id="examenes"><i class="fas fa-eye"></i></button></a></td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
        </div>
    </div>
</body>