<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<div class="login100-pic js-tilt" data-tilt>
				<img src="<?php echo base_url().'assets/login/images/hp.png';?>" alt="Member login">
			</div>

			<form action="<?php echo base_url();?>LoginController/auth" method="post" class="login100-form validate-form" autocomplete="off">
				<span class="login100-form-title">
					Iniciar sesión
				</span>

				<div class="wrap-input100 validate-input" data-validate = "Usuario requerido">
					<input class="input100" type="email" name="correo" id="correo" placeholder="Correo">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-user" aria-hidden="true"></i>
					</span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Contraseña requerida">
					<input class="input100" type="password" name="contrasena" id="contrasena" placeholder="Contraseña">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
				</div>

				<div class="container-login100-form-btn">
					<button type="submit" name="login" class="login100-form-btn">
						Login
					</button>
				</div>

				<div class="text-center p-t-136">
					
				</div>
				
			</form>
		</div>
	</div>
</div>