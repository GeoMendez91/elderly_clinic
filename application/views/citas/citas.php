<body>
    <div class="container" id="formulario">
        <div>
    <h3><i class="fas fa-calendar-plus verde"></i> | Citas Programadas </h3>
			<p>Aqui revisar las citas que estan activas.</p>
            <hr>
            <?php if ($this->session->userdata('rol') != 3 ){ ?>
			<a href="<?php echo base_url('CitasController/CitasForm') ?>"><button class="btn btn-success"><i class="fas fa-calendar-plus verde"></i> Programar cita</button></a>
           <?php } ?>
        <div>
        <table class="table table-light" id="citastab">
            <thead class="bg-primary">
                <tr>
                    <th class="text-center">Paciente</th>
                    <th class="text-center">Dui</th>
                    <th class="text-center">fecha</th>
                    <th class="text-center">hora</th>
                    <th class="text-center">Doctor</th>
                    <th class="text-center">Especialidad</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($citas as $c) { ?>
                <tr>
                    <td align="center"><?php echo $c->nombre." ".$c->apellido ?></td>
                    <td align="center"><?php echo $c->dui ?></td>
                    <td align="center"><?php echo $c->fecha ?></td>
                    <td align="center"><?php echo date('H:i A', strtotime($c->hora)) ?></td>
                    <td align="center"><?php echo $c->nombre_medico." ".$c->apellido_medico ?></td>
                    <td align="center"><?php echo $c->especialidad ?></td>
                    <td align="center"><?php echo $c->estado ?></td>
                    <td align="center">
                        <?php if ($c->estado == "cancelada" || $this->session->userdata('rol') == 3) {  ?>
                            <button class="btn btn-circle btn-primary" disabled=""><i class="fas fa-sync-alt"></i></button>
                        <button class="btn btn-circle btn-danger" disabled=""><i class="fas fa-trash"></i></button>
                        <?php }else{ ?>
                        <a href="<?php echo base_url('CitasController/CitasFormUpdate/').$c->idcita ?>"><button class="btn btn-circle btn-primary"><i class="fas fa-sync-alt"></i></button></a>
                        <a href="<?php echo base_url('CitasController/deleteCita/').$c->idcita ?>"><button class="btn btn-circle btn-danger"><i class="fas fa-trash"></i></button></a>
                        <?php } ?>
                    </td>
                </tr>
          <?php  } ?>
            </tbody>
        </table>
        </div>
    </div>
</body>
