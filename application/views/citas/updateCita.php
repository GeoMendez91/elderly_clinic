<body>
    <div class="container" id="formulario">
    <h3><i class="fas fa-calendar-plus verde"></i> | Programar una cita </h3>
			<p>Aqui puedes programar una cita llenando los campos siguientes.</p>
			<hr>
           <center>
        <table class="col-md-6">
            <tbody>
                <tr>
                    <td>DUI paciente</td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="idpaciente" value="<?= $cita->dui ?>" placeholder="Ingrese un DUI sin guiones" id="dui" class="form-control">
                            <div id="duiCita" class="text-danger">El DUI es requerido</div>
                        <div id="duiCita2" class="text-danger">Ingrese un DUI válido</div>
                        <input type="hidden" id="idcita" value="<?= $cita->idcita ?>">
                    </td>
                </tr>
                <tr style="height: 25px"></tr>
                <tr>
                    <td>Fecha</td>
                </tr>
                <tr>
                    <td>
                    <input type="date" name="fecha" id="fecha" value="<?= $cita->fecha ?>" min="<?php echo $fecha = date('Y-m-d'); ?>" class="form-control">
                    <div id="fechaCita" class="text-danger">Seleccione una fecha</div>
                    </td>
                </tr>
                <tr style="height: 25px"></tr>
                 <tr>
                    <td>Hora</td>
                </tr>
                <tr>
                    <td>
                        <select class="form-control" id="hora" name="hora">
                            <option>--Seleccione una hora--</option>
                        
                        <?php   $ini = new DateTime('08:00'); 
                                $fin = new DateTime('17:00');
                                $interval = DateInterval::createFromDateString('30 min');
                                $horas = new DatePeriod($ini, $interval, $fin);
                                foreach ($horas as $hora)
                                { if ( date('H:i', strtotime($cita->hora)) == $hora->format('H:i')){  ?>
                                <option value="<?php echo $hora->format('H:i') ?>" selected><?php echo $hora->format('H:i a') ?></option>
                                <?php  }else{ ?>
                                    <option value="<?php echo $hora->format('H:i') ?>"><?php echo $hora->format('H:i a') ?></option>
                                <?php  }} ?>
                                </select>
                                <div id="horaCita" class="text-danger">Seleccione una hora</div>
                   <!-- <input type="time" name="hora" id="hora" min="" class="form-control"> -->
                    </td>
                </tr>
                <tr style="height: 25px"></tr>
                 <tr>
                    <td>Doctor</td>
                </tr>
                <tr>
                    <td>
                    <select class="form-control" id="idmedico" name="iddoctor">
                        <option value="">--Seleccione un médico--</option>
                        <?php foreach ($medico as $medico) { 
                            if ($medico->idmedico == $cita->idmedico){  ?>
                            <option value="<?php echo $medico->idmedico ?>" selected><?php echo $medico->nombre_medico." ".$medico->apellido_medico." ".$medico->especialidad ?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $medico->idmedico ?>"><?php echo $medico->nombre_medico." ".$medico->apellido_medico." ".$medico->especialidad ?></option>
                        <?php }} ?>
                    </select>
                    <div id="medicoCita" class="text-danger">Seleccione un médico</div>
                    </td>
                </tr>
                <tr style="height: 25px"></tr>
                 <tr>
                    <td>
                    <button class="btn btn-primary col-md-12" id="update_cita">Reprogramar cita</button>
                    </td>
                </tr>
                <tr style="height: 30px"></tr>
            </tbody>
        </table>
        <img class="col-md-6" style="border-radius: 20px" src="<?php echo base_url('assets/img/cita.jpg') ?>">
        <br><br>
        </center>
    </div>
</body>