var numerosCarcteres="0123456789!#$%&'()*+,-./:;<=>?@[\]^_`{|}~";

function tiene_numeros(cadena){
	for(i=0; i<cadena.length; i++){
	   if (numerosCarcteres.indexOf(cadena.charAt(i),0)!=-1){
		  return true;
	   }
	}
	return false;
 } 

$('#registrar_paciente').click(function(){
	var nombre = $('#nombre').val(),
		dui = $('#dui').val(),
		apellido = $('#apellido').val(),
		edad = $('#edad').val(),
		idsexo = $('#idsexo').val(),
		direccion = $('#direccion').val(),
		telefono = $('#telefono').val(),
		nombre_con = $('#nombre_con').val(),
		apellido_con = $('#apellido_con').val(),
		direccion_con = $('#direccion_con').val(),
		correo = $('#correo').val(),
		contrasena = $('#contrasena').val(),
		parentesco = $('#parentesco').val(),
		telefono_fijo = $('#telefono_fijo').val(),
		telefono_movil = $('#telefono_movil').val();


		if (dui == "") {
			$('#duiPaciente').fadeIn();
			return false;
		}else{
			$('#duiPaciente').fadeOut('');
		}
		if (isNaN(dui) == true || dui.length != 9) {
			$('#duiPaciente2').fadeIn();
			return false;
		}else{
			$('#duiPaciente2').fadeOut();
		}
		if (nombre == "") {
			$('#nombrePaciente').fadeIn();
			return false;
		}else{
			$('#nombrePaciente').fadeOut('');
		}
		if (tiene_numeros(nombre) == true) {
			$('#nombrePaciente2').fadeIn();
			return false;
		}else{
			$('#nombrePaciente2').fadeOut();
		}
		if (apellido == "") {
			$('#apellidoPaciente').fadeIn();
			return false;
		}else{
			$('#apellidoPaciente').fadeOut();
		}
		if (tiene_numeros(apellido) == true) {
			$('#apellidoPaciente2').fadeIn();
			return false;
		}else{
			$('#apellidoPaciente2').fadeOut();
		}
		if (edad == "") {
			$('#edadPaciente').fadeIn();
			return false;
		}else{
			$('#edadPaciente').fadeOut();
		}
		if (isNaN(edad) || edad > 99) {
			$('#edadPaciente2').fadeIn();
			return false;
		}else{
			$('#edadPaciente2').fadeOut();
		}
		if (idsexo == "") {
			$('#sexoPaciente').fadeIn();
			return false;
		}else{
			$('#sexoPaciente').fadeOut();
		}
		if (direccion == "") {
			$('#direccionPaciente').fadeIn();
			return false;
		}else{
			$('#direccionPaciente').fadeOut();
		}
		if (telefono == "") {
			$('#telefonoPaciente').fadeIn();
			return false;
		}else{
			$('#telefonoPaciente').fadeOut();
		}
		if (isNaN(telefono) == true || telefono.length != 8) {
			$('#telefonoPaciente2').fadeIn();
			return false;
		}else{
			$('#telefonoPaciente2').fadeOut();
		}
		if (nombre_con == "") {
			$('#nombreContacto').fadeIn();
			return false;
		}else{
			$('#nombreContacto').fadeOut();
		}
		if (apellido_con == "") {
			$('#apellidoContacto').fadeIn();
			return false;
		}else{
			$('#apellidoContacto').fadeOut();
		}
		if($("#correo").val().indexOf('@', 0) == -1 || $("#correo").val().indexOf('.', 0) == -1) {
            $('#correoContacto2').fadeIn();
            return false;
        }else{
        	$('#correoContacto2').fadeOut();
		}
		if (correo == "") {
			$('#contraContacto').fadeIn();
			return false;
		}else{
			$('#contraContacto').fadeOut();
		}
		if (contrasena == "") {
			$('#contraContacto').fadeIn();
			return false;
		}else{
			$('#contraContacto').fadeOut();
		}
		if (direccion_con == "") {
			$('#direccionContacto').fadeIn();
			return false;
		}else{
			$('#direccionContacto').fadeOut();
		}
		if (parentesco == "") {
			$('#parentescoContacto').fadeIn();
			return false;
		}else{
			$('#parentescoContacto').fadeOut();
		}
		if (telefono_fijo == "") {
			$('#telFijo').fadeIn();
			return false;
		}else{
			$('#telFijo').fadeOut();
		}
		if (isNaN(telefono_fijo) == true || telefono_fijo.length != 8) {
			$('#telFijo2').fadeIn();
			return false;
		}else{
			$('#telFijo2').fadeOut();
		}
		if (telefono_movil == "") {
			$('#telMovil').fadeIn();
			return false;
		}else{
			$('#telMovil').fadeOut();
		}
		if (isNaN(telefono_movil) == true || telefono_movil.length != 8) {
			$('#telMovil2').fadeIn();
			return false;
		}else{
			$('#telMovil2').fadeOut();
		}


		$.ajax({

			dataType: 'json',
			url: baseurl+'ExpedienteController/insertarPaciente',
			type: 'post',
			dataType: 'json',
			data:{
				dui,nombre,apellido,edad,idsexo,direccion,telefono,nombre_con,apellido_con,direccion_con,correo,contrasena,parentesco,telefono_fijo,telefono_movil
			},
			dataType: 'json',
			success: function(data){
				if (data.success === 1) {
					Swal.fire({
					  position: 'top-end',
					  type: 'success',
					  title: 'Registros guardados exitosamente!',
					  showConfirmButton: false,
					  timer: 1500
					});
					document.location.href=baseurl+'ExpedienteController';

				}else{
					Swal.fire({
						position: 'top-end',
						type: 'error',
						title: 'Error al ingresar resgistros!',
						showConfirmButton: false,
						timer: 1500
				});
				document.location.href=baseurl+'ExpedienteController'

			}

		},
		error: function(e){
			console.log(e);
			Swal.fire({
				type: 'error',
				title: 'Oops...',
				text: 'Algo ocurrio! Consulte con el administrador',
				footer: '<a href>Why do I have this issue?</a>'
			});
		}
	});
});

/* Modulo de USUARIOS */
$('#agrega_usuario').click(function(){
	var correo = $('#correo').val(),
	nombre = $('#nombre').val(),
	apellido = $('#apellido').val(),
	contrasena = $('#contrasena').val(),
	id_rol = $('#id_rol').val();

	$.ajax({
		dataType:'json',
		url: baseurl + '/UsuariosController/agregar',
		type: 'post',
		dataType: 'json',
		data:{
			correo,nombre,apellido,contrasena,id_rol
		},
		dataType:'json',
		beforesend: function(){
			//gif de carga
		},
		success: function(data){
			if(data.success === 1){
				alert('Registros almacenados');
				document.location.href = baseurl + '/UsuariosController';
			}else{
				alert('No se almacenaron los datos');
			}
		},
		error: function(e){
			alert('Ocurrió un error al momento de agregar');
			console.log(e);
		}
	});
});

$('#registrar_medico').click(function(){
	var nombre_medico = $('#nombre_medico').val(),
		dui_medico = $('#dui_medico').val(),
		apellido_medico = $('#apellido_medico').val(),
		correo = $('#correo').val(),
		contrasena = $('#contrasena').val(),
		idespecialidad = $('#idespecialidad').val();
		
		if(dui_medico == "") {
			$('#duiMedico').fadeIn();
			return false;
		}else{
			$('#duiMedico').fadeOut();
		}
		if (isNaN(dui_medico) == true || dui_medico.length != 9) {
			$('#duiMedico2').fadeIn();
			return false;
		}else{
			$('#duiMedico2').fadeOut();
		}
		if (nombre_medico == "") {
			$('#nombreMedico').fadeIn();
			return false;
		}else{
			$('#nombreMedico').fadeOut();
		}
		if (tiene_numeros(nombre_medico) == true) {
			$('#nombreMedico2').fadeIn();
			return false;
		}else{
			$('#nombreMedico2').fadeOut();
		}
		if (apellido_medico == "") {
			$('#apellidoMedico').fadeIn();
			return false;
		}else{
			$('#apellidoMedico').fadeOut();
		}
		if (tiene_numeros(apellido_medico) == true) {
			$('#apellidoMedico2').fadeIn();
			return false;
		}else{
			$('#apellidoMedico2').fadeOut();
		}
		if($("#correo").val().indexOf('@', 0) == -1 || $("#correo").val().indexOf('.', 0) == -1) {
            $('#correoMedico').fadeIn();
            return false;
        }else{
        	$('#correoMedico').fadeOut();
		}
		if(correo == "") {
            $('#correoMedico2').fadeIn();
            return false;
        }else{
        	$('#correoMedico2').fadeOut();
        }
		if (contrasena == "") {
			$('#contraMedico').fadeIn();
			return false;
		}else{
			$('#contraMedico').fadeOut();
		}
		if (idespecialidad < 1) {
			$('#especialidadMedico').fadeIn();
			return false;
		}else{
			$('#especialidadMedico').fadeOut();
		}
		

		$.ajax({
			dataType: 'json',
			url: baseurl+'MedicosController/ingresarMedico',
			type: 'post',
			dataType: 'json',
			data:{
				dui_medico,nombre_medico,apellido_medico,correo,contrasena,idespecialidad
			},
			dataType: 'json',
			success: function(data){
				if (data.success === 1){
					Swal.fire({
						position: 'top-end',
						type: 'success',
						title: 'Registros guardados exitosamente!',
						showConfirmButton: false,
						timer: 1500
					  });
					  document.location.href=baseurl+'/MedicosController/getMedico'
				}else{
					Swal.fire({
						position: 'top-end',
						type: 'error',
						title: 'Error al ingresar resgistros!',
						showConfirmButton: false,
						timer: 1500
				});
				}
			},
			error: function(e){
				console.log(e);
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'Algo ocurrio! Consulte con el administrador',
					footer: '<a href>Why do I have this issue?</a>'
				  });
			}
		});

});

$('#registrar_contacto').click(function(){
	var nombre_con = $('#nombre_con').val(),
		apellido_con = $('#apellido_con').val(),
		correo = $('#correo').val(),
		contrasena = $('#contrasena').val(),
		direccion_con = $('#direccion_con').val(),
		parentesco = $('#parentesco').val(),
		telefono_fijo = $('#telefono_fijo').val(),
		telefono_movil = $('#telefono_movil').val(),
		expediente = $('#expediente').val();

		if (nombre_con == "") {
			$('#nombreContacto').fadeIn();
			return false;
		}else{
			$('#nombreContacto').fadeOut();
		}
		if (tiene_numeros(nombre_con) == true) {
			$('#nombreContacto2').fadeIn();
			return false;
		}else{
			$('#nombreContacto2').fadeOut();
		}
		if (apellido_con == "") {
			$('#apellidoContacto').fadeIn();
			return false;
		}else{
			$('#apellidoContacto').fadeOut();
		}
		if (tiene_numeros(apellido_con) == true) {
			$('#apellidoContacto2').fadeIn();
			return false;
		}else{
			$('#apellidoContacto2').fadeOut();
		}
		if($("#correo").val().indexOf('@', 0) == -1 || $("#correo").val().indexOf('.', 0) == -1) {
            $('#correoContacto').fadeIn();
            return false;
        }else{
        	$('#correoContacto').fadeOut();
        }
		if (contrasena == "") {
			$('#contraContacto').fadeIn();
			return false;
		}else{
			$('#contraContacto').fadeOut();
		}
		if (direccion_con == "") {
			$('#direccionContacto').fadeIn();
			return false;
		}else{
			$('#direccionContacto').fadeOut();
		}
		if (parentesco == "") {
			$('#parentescoContacto').fadeIn();
			return false;
		}else{
			$('#parentescoContacto').fadeOut();
		}
		if (tiene_numeros(parentesco) == true) {
			$('#parentescoContacto2').fadeIn();
			return false;
		}else{
			$('#parentescoContacto2').fadeOut();
		}
		if (telefono_fijo == "") {
			$('#telfijo').fadeIn();
			return false;
		}else{
			$('#telfijo').fadeOut();
		}
		if (isNaN(telefono_fijo == true || telefono_fijo.length != 8)) {
			$('#telfijo2').fadeIn();
			return false;
		}else{
			$('#telfijo2').fadeOut();
		}
		if (telefono_movil == "") {
			$('#telmovil').fadeIn();
			return false;
		}else{
			$('#telmovil').fadeOut();
		}
		if (isNaN(telefono_movil == true || telefono_movil.length != 8)) {
			$('#telmovil2').fadeIn();
			return false;
		}else{
			$('#telmovil2').fadeOut();
		}

		$.ajax({
			dataType: 'json',
			url: baseurl+'FamiliaresController/agregarContacto',
			type: 'post',
			dataType: 'json',
			data:{
				nombre_con,apellido_con,correo,contrasena,direccion_con,parentesco,telefono_fijo,telefono_movil,expediente
			},
			dataType: 'json',
			success: function(data){
				if (data.success === 1){
					Swal.fire({
						position: 'top-end',
						type: 'success',
						title: 'Registros guardados exitosamente!',
						showConfirmButton: false,
						timer: 2500
					  });
					  document.location.href=baseurl+'ExpedienteController'
				}else{
					Swal.fire({
						position: 'top-end',
						type: 'error',
						title: 'Error al ingresar resgistros!',
						showConfirmButton: false,
						timer: 2500
				});
				}
			},
			error: function(e){
				console.log(e);
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'Algo ocurrio! Consulte con el administrador',
					footer: '<a href>Why do I have this issue?</a>'
				  });
			}
		});

});

$('#solicitar_cita').click(function(){
	var dui = $('#dui').val(),
		fecha = $('#fecha').val(),
		hora = $('#hora').val(),
		idmedico = $('#idmedico').val();

		if (dui == "") {
			$('#duiCita').fadeIn();
			return false;
		}else{
			$('#duiCita').fadeOut();
		}
		if (isNaN(dui) == true) {
			$('#duiCita2').fadeIn();
			return false;
		}else{
			$('#duiCita2').fadeOut();
		}
		if (fecha == "") {
			$('#fechaCita').fadeIn();
			return false;
		}else{
			$('#fechaCita').fadeOut();
		}
		
		if (hora == "") {
			$('#horaCita').fadeIn();
			return false;
		}else{
			$('#horaCita').fadeOut();
		}
		if (idmedico == "") {
			$('#medicoCita').fadeIn();
			return false;
		}else{
			$('#medicoCita').fadeOut();
		}
		

		$.ajax({
			dataType: 'json',
			url: baseurl+'CitasController/storeCitas',
			type: 'post',
			dataType: 'json',
			data:{
				dui,fecha,hora,idmedico
			},
			dataType: 'json',
			success: function(data){
				if (data.success === 1){
					document.location.href=baseurl+'ExpedienteController';
					Swal.fire({
						position: 'top-end',
						type: 'success',
						title: 'Cita registrada exitosamente!',
						showConfirmButton: false,
						timer: 2500
					  });
					  
				}else if (data.success === "ya tienes una cita"){
					Swal.fire({
						position: 'top-end',
						type: 'error',
						title: 'Ya tienes una cita programada en ese horario!',
						showConfirmButton: false,
						timer: 2500
				});
				} else if (data.success === "horario no disponible" || data.success === "domingo"){
					Swal.fire({
						position: 'top-end',
						type: 'error',
						title: 'Horario no disponible, selecciona otro horario!',
						showConfirmButton: false,
						timer: 2500
					});
				} else if (data.success === "no resgitrado"){
					Swal.fire({
						position: 'top-end',
						type: 'error',
						title: 'Paciente no registrado!',
						showConfirmButton: false,
						timer: 2500
					});
				}
			},
			error: function(e){
				console.log(e);
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'Algo ocurrio! Consulte con el administrador',
					
				  });
			}
		});

});

$('#save-consulta').click(function(){
	var diagnosticos = $('#diagnosticos').val(),
		id_expediente = $('#idexpediente').val();
		

		if (diagnosticos == "") {
			$('#diagnostico').fadeIn();
			return false;
		}else{
			$('#diagnostico').fadeOut();
		}
		

		$.ajax({
			dataType: 'json',
			url: baseurl+'ConsultasController/storeDiagnostic',
			type: 'post',
			dataType: 'json',
			data:{
				diagnosticos, id_expediente
			},
			dataType: 'json',
			success: function(data){
				if (data.success === 1){
					document.location.href=baseurl+'ConsultasController/getDiagnosticos';
					Swal.fire({
						position: 'top-end',
						type: 'success',
						title: 'Cita registrada exitosamente!',
						showConfirmButton: false,
						timer: 2500
					  });
					  
				}
				
			},
			error: function(e){
				console.log(e);
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'Algo ocurrio! Consulte con el administrador',
				
				  });
			}
		});

});